   ____      _ _     _ _ _            
  / ___|__ _| (_) __| (_) |_ __ _ ___ 
 | |   / _` | | |/ _` | | __/ _` / __|
 | |__| (_| | | | (_| | | || (_| \__ \
  \____\__,_|_|_|\__,_|_|\__\__,_|___/
by Rafał Kosyl, admin@rafkos.net                                      

##################################################################

OPTIONAL FEATURES

Currently there are two optional features available in the 
plugin. The features are enabled by default. They can be disabled
inside the misc_config.json file.

# Burning furnaces feature

This feature allows furnaces to burn (have the burning animation)
when fuel materials are provided but no smelting is being done.
The feature does not consume fuel items. The feature is protected
against exploiting - when smelting material is provided the
furnace works just as in regular Minecraft.

# Fire stocking feature

The plugin was created before fireplaces were added to the main
game. This feature was designed to allow for maintaining fire 
by throwing sticks or other fuel materials into it.