   ____      _ _     _ _ _            
  / ___|__ _| (_) __| (_) |_ __ _ ___ 
 | |   / _` | | |/ _` | | __/ _` / __|
 | |__| (_| | | | (_| | | || (_| \__ \
  \____\__,_|_|_|\__,_|_|\__\__,_|___/
by Rafał Kosyl, admin@rafkos.net                                      

##################################################################

HOW TO CREATE A FACTOR

Create a new file with ".factor" extension inside you world's 
factors folder. A template of factor's file is available below.
You can also copy an existing one.

{
  "factorName": "BOOTS_PROTECTING_FROM_COLD",
  "tempChange": 0.35,  
  "tempUnit": "CELSIUS",
  "condition":  
  """
  !(LOWER_BODY_IN_WATER)
  & (COLD_BIOME & WEARING_LEATHER_BOOTS)
  """
}

# Description of parameters

  factorName - a locale key defined in locale_xx_xx.json, used to
  get a display name of the factor.

  tempChange - number applied to player's target body temperature
  when given factor is activated.

  tempUnit - unit of the tempChange value. Can be either CELSIUS or
  FAHRENHEIT. It does not impact the way it is displayed in game.

  condition - this value can be described as a multiline string.
  Condition represents a boolean expression which - when true -
  allows factor to be applied to specific player.

# How to create a condition?

  This is a simplified boolean expression. You can use these
  operators: &, |, !, (, ). These are respectively: and, or, 
  negation, opening bracket, closing bracket. Examples of 
  conditions and their explanations:

  LOWER_BODY_IN_WATER & DRY_BIOME
  - simple, applies only when both flags are present (true).

  !(LOWER_BODY_IN_WATER) & COLD_BIOME & WEARING_IRON_BOOTS
  - applies only when player's feet are not in water and the 
  biome is cold and player has iron boots.

  !(LOWER_BODY_IN_WATER | UPPER_BODY_IN_WATER) 
  & SNOWY_BIOME & WEARING_DIAMOND_LEGGINGS
  - applies only when player's feet or head are not in water
  and the biome is snowy and player is wearing diamond
  leggings.