{
	"factorName": "FOOD_CONSUMED",
	"tempChange": 0.35,
	"tempUnit": "CELSIUS",
	"condition": 
"""
(COLD_BIOME | SNOWY_BIOME) & CONSUMED_FOOD_OR_DRINK
"""
}