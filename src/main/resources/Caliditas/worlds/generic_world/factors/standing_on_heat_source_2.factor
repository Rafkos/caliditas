{
	"factorName": "STANDING_ON_HEAT_SOURCE",
	"tempChange": 2.0,
	"tempUnit": "CELSIUS",
	"condition":
"""
!UPPER_BODY_IN_WATER
& (STANDING_ON_HEAT_SOURCE & COLD_BIOME)
"""
}