{
	"factorName": "CHILL_FROM_GROUND",
	"tempChange": -1.15,
	"tempUnit": "CELSIUS",
	"condition":
"""
(LOW_ALTITUDE & IN_CLOSED_SPACE & DRY_BIOME)
"""
}