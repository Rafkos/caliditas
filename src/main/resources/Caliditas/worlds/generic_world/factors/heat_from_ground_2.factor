{
	"factorName": "HEAT_FROM_GROUND",
	"tempChange": 1.55,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(UPPER_BODY_IN_WATER | LOWER_BODY_IN_WATER | UPPER_BODY_IN_WATER)
& (LOW_ALTITUDE & IN_CLOSED_SPACE & COLD_BIOME)
"""
}