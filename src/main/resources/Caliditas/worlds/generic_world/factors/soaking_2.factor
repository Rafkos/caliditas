{
	"factorName": "SOAKING",
	"tempChange": -0.35,
	"tempUnit": "CELSIUS",
	"condition": 
"""
!(UNDER_ROOF | UPPER_BODY_IN_WATER)
& (RAINING_OR_SNOWING_ON_PLAYER & SNOWY_BIOME)
"""
}