{
	"factorName": "SOAKING",
	"tempChange": -0.25,
	"tempUnit": "CELSIUS",
	"condition": 
"""
!(UNDER_ROOF | UPPER_BODY_IN_WATER)
& (RAINING_OR_SNOWING_ON_PLAYER & COLD_BIOME)
"""
}