{
	"factorName": "CHILL_FROM_SHADOW",
	"tempChange": -1.85,
	"tempUnit": "CELSIUS",
	"condition":
"""
(DRY_BIOME & IN_CLOSED_SPACE & UNDER_ROOF)
"""
}