{
	"factorName": "CHESTPLATE_PROTECTING_FROM_COLD",
	"tempChange": 0.15,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(UPPER_BODY_IN_WATER)
& (COLD_BIOME & WEARING_DIAMOND_CHESTPLATE)
"""
}