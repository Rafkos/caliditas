{
	"factorName": "BOOTS_PROTECTING_FROM_COLD",
	"tempChange": 0.01,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(LOWER_BODY_IN_WATER)
& (SNOWY_BIOME & WEARING_CHAINMAIL_BOOTS)
"""
}