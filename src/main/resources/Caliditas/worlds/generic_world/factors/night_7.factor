{
	"factorName": "NIGHT",
	"tempChange": -0.4,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(IN_CLOSED_SPACE | INFLUENCED_BY_ANY_HEAT_SOURCE)
& (MIDNIGHT & WARM_BIOME)
"""
}