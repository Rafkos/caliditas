{
	"factorName": "SOAKING",
	"tempChange": -0.2,
	"tempUnit": "CELSIUS",
	"condition": 
"""
!(UNDER_ROOF | UPPER_BODY_IN_WATER | COLD_BIOME | SNOWY_BIOME | NETHER | THE_END | THE_VOID)
& RAINING_OR_SNOWING_ON_PLAYER
"""
}