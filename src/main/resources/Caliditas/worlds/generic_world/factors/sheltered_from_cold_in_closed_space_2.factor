{
	"factorName": "SHELTERED_FROM_COLD_IN_CLOSED_SPACE",
	"tempChange": 1.1,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(UPPER_BODY_IN_WATER | LOWER_BODY_IN_WATER)
& (SNOWY_BIOME & IN_CLOSED_SPACE)
"""
}