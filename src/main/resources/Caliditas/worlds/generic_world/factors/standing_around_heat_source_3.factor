{
	"factorName": "STANDING_AROUND_HEAT_SOURCE",
	"tempChange": 1.55,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(UPPER_BODY_IN_WATER | LOWER_BODY_IN_WATER)
& (STANDING_AROUND_HEAT_SOURCE & SNOWY_BIOME)
"""
}