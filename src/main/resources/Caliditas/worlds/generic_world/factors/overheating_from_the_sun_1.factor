{
	"factorName": "OVERHEATING_FROM_THE_SUN",
	"tempChange": 0.25,
	"tempUnit": "CELSIUS",
	"condition": 
"""
!(STORM | THUNDERING | UNDER_ROOF)
& (WEARING_NO_HELMET & DAY & DRY_BIOME)
"""
}