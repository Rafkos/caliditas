{
	"factorName": "NIGHT",
	"tempChange": -1.05,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(INFLUENCED_BY_ANY_HEAT_SOURCE)
& (NIGHT & DRY_BIOME & IN_CLOSED_SPACE)
"""
}