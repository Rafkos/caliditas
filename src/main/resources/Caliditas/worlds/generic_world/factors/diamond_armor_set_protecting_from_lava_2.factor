{
	"factorName": "DIAMOND_ARMOR_SET_PROTECTING_FROM_LAVA",
	"tempChange": -1.35,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(STANDING_IN_LAVA)
& (WEARING_DIAMOND_CHESTPLATE & STANDING_NEAR_LAVA)
"""
}