{
	"factorName": "CHILL_FROM_SHADOW",
	"tempChange": -0.25,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(IN_CLOSED_SPACE)
& (WARM_BIOME & UNDER_ROOF)
"""
}