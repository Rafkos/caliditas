{
	"factorName": "CHILL_FROM_SHADOW",
	"tempChange": -1.85,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(UNDER_ROOF)
& (DRY_BIOME & STORM)
"""
}