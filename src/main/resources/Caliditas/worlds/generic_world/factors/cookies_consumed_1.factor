{
	"factorName": "FAT_PROTECTING_FROM_COLD",
	"tempChange": 0.20,
	"tempUnit": "CELSIUS",
	"condition": 
"""
(COLD_BIOME | SNOWY_BIOME)
& (CONSUMED_COOKIE | CONSUMED_CAKE | CONSUMED_PUMPKIN_PIE)
"""
}