{
	"factorName": "STANDING_NEAR_HEAT_SOURCE",
	"tempChange": 1.95,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(UPPER_BODY_IN_WATER | LOWER_BODY_IN_WATER)
& (STANDING_NEAR_HEAT_SOURCE & SNOWY_BIOME)
"""
}