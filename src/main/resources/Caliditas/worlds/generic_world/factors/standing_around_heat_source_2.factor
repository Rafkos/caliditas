{
	"factorName": "STANDING_AROUND_HEAT_SOURCE",
	"tempChange": 1.45,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(UPPER_BODY_IN_WATER | LOWER_BODY_IN_WATER)
& (STANDING_AROUND_HEAT_SOURCE & COLD_BIOME)
"""
}