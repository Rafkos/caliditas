{
	"factorName": "CHESTPLATE_PROTECTING_FROM_COLD",
	"tempChange": 0.25,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(UPPER_BODY_IN_WATER)
& (SNOWY_BIOME & WEARING_IRON_CHESTPLATE)
"""
}