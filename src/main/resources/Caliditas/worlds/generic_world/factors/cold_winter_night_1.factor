{
	"factorName": "COLD_WINTER_NIGHT",
	"tempChange": -0.8,
	"tempUnit": "CELSIUS",
	"condition": 
"""
!(IN_CLOSED_SPACE | INFLUENCED_BY_ANY_HEAT_SOURCE)
& (COLD_BIOME & NIGHT)
"""
}