{
	"factorName": "DRANK_WATER",
	"tempChange": -0.65,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(STORM | THUNDERING | UNDER_ROOF)
& (DAY & DRY_BIOME & CONSUMED_POTION_WATER)
"""
}