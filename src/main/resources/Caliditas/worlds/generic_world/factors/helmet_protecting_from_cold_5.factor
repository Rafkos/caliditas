{
	"factorName": "HELMET_PROTECTING_FROM_COLD",
	"tempChange": 0.05,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(UPPER_BODY_IN_WATER)
& (COLD_BIOME & WEARING_CHAINMAIL_HELMET)
"""
}