{
	"factorName": "HEAT_FROM_TORCH",
	"tempChange": 0.55,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(UPPER_BODY_IN_WATER | LOWER_BODY_IN_WATER)
& (HEAT_FROM_TORCH & SNOWY_BIOME)
"""
}