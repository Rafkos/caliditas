{
	"factorName": "LEGGINGS_PROTECTING_FROM_COLD",
	"tempChange": 0.1,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(LOWER_BODY_IN_WATER)
& (COLD_BIOME & WEARING_IRON_LEGGINGS)
"""
}