{
	"factorName": "LEGGINGS_PROTECTING_FROM_COLD",
	"tempChange": 0.45,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(LOWER_BODY_IN_WATER)
& (SNOWY_BIOME & WEARING_LEATHER_LEGGINGS)
"""
}