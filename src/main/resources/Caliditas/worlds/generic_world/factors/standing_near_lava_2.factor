{
	"factorName": "STANDING_NEAR_LAVA",
	"tempChange": 2.3,
	"tempUnit": "CELSIUS",
	"condition": 
"""
!(UPPER_BODY_IN_WATER)
& (STANDING_NEAR_LAVA & COLD_BIOME)
"""
}