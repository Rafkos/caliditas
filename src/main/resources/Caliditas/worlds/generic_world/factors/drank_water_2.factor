{
	"factorName": "DRANK_WATER",
	"tempChange": -0.45,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(STORM | THUNDERING | UNDER_ROOF)
& (DAY & WARM_BIOME & CONSUMED_POTION_WATER)
"""
}