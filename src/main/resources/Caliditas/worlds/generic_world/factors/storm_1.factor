{
	"factorName": "STORM",
	"tempChange": -0.25,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(IN_CLOSED_SPACE | SNOWY_BIOME | COLD_BIOME | DRY_BIOME)
& STORM
"""
}