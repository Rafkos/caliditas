{
	"factorName": "COLD_WINTER_NIGHT",
	"tempChange": -1.0,
	"tempUnit": "CELSIUS",
	"condition": 
"""
!(IN_CLOSED_SPACE | INFLUENCED_BY_ANY_HEAT_SOURCE)
& (SNOWY_BIOME & NIGHT)
"""
}