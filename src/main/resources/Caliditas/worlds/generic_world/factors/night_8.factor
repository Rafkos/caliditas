{
	"factorName": "NIGHT",
	"tempChange": -0.35,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(IN_CLOSED_SPACE | INFLUENCED_BY_ANY_HEAT_SOURCE)
& (MIDNIGHT & MODERATE_BIOME)
"""
}