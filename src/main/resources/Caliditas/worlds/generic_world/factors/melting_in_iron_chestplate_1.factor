{
	"factorName": "MELTING_IN_IRON_CHESTPLATE",
	"tempChange": 0.75,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(UNDER_ROOF | STORM)
& (WEARING_IRON_CHESTPLATE & DRY_BIOME & DAY)
"""
}