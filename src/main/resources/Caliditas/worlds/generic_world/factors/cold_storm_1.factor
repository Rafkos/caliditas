{
	"factorName": "COLD_STORM",
	"tempChange": -0.8,
	"tempUnit": "CELSIUS",
	"condition": 
"""
!(IN_CLOSED_SPACE)
& (COLD_BIOME & STORM)
"""
}