{
	"factorName": "NIGHT",
	"tempChange": -0.25,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(IN_CLOSED_SPACE | INFLUENCED_BY_ANY_HEAT_SOURCE)
& (NIGHT & MODERATE_BIOME)
"""
}