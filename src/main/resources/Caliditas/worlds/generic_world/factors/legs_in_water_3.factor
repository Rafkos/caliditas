{
	"factorName": "LEGS_IN_WATER",
	"tempChange": -0.1,
	"tempUnit": "CELSIUS",
	"condition": 
"""
!(COLD_BIOME | SNOWY_BIOME | DRY_BIOME | WARM_BIOME)
& LOWER_BODY_IN_WATER
"""
}