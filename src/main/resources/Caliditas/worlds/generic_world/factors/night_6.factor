{
	"factorName": "NIGHT",
	"tempChange": -1.35,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(INFLUENCED_BY_ANY_HEAT_SOURCE)
& (MIDNIGHT & DRY_BIOME & IN_CLOSED_SPACE)
"""
}