{
	"factorName": "BOOTS_PROTECTING_FROM_COLD",
	"tempChange": 0.15,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(LOWER_BODY_IN_WATER)
& (SNOWY_BIOME & WEARING_IRON_BOOTS)
"""
}