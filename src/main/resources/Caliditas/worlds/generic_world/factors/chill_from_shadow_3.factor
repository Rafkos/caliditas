{
	"factorName": "CHILL_FROM_SHADOW",
	"tempChange": -0.95,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(IN_CLOSED_SPACE)
& (DRY_BIOME & UNDER_ROOF)
"""
}