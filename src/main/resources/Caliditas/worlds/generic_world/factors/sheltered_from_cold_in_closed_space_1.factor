{
	"factorName": "SHELTERED_FROM_COLD_IN_CLOSED_SPACE",
	"tempChange": 0.6,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(UPPER_BODY_IN_WATER | LOWER_BODY_IN_WATER)
& (COLD_BIOME & IN_CLOSED_SPACE)
"""
}