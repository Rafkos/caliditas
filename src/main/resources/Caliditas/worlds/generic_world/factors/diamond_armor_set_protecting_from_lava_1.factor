{
	"factorName": "DIAMOND_ARMOR_SET_PROTECTING_FROM_LAVA",
	"tempChange": -2.35,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(STANDING_NEAR_LAVA)
& (WEARING_DIAMOND_CHESTPLATE & STANDING_IN_LAVA)
"""
}