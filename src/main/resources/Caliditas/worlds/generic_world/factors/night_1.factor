{
	"factorName": "NIGHT",
	"tempChange": -1.5,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(IN_CLOSED_SPACE | INFLUENCED_BY_ANY_HEAT_SOURCE)
& (NIGHT & DRY_BIOME)
"""
}