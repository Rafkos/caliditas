{
	"factorName": "STANDING_NEAR_LAVA",
	"tempChange": 1.35,
	"tempUnit": "CELSIUS",
	"condition": 
"""
!(COLD_BIOME | SNOWY_BIOME | UPPER_BODY_IN_WATER)
& STANDING_NEAR_LAVA
"""
}