{
	"factorName": "CHESTPLATE_PROTECTING_FROM_COLD",
	"tempChange": 0.65,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(UPPER_BODY_IN_WATER)
& (SNOWY_BIOME & WEARING_LEATHER_CHESTPLATE)
"""
}