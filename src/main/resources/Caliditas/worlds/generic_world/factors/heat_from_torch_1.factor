{
	"factorName": "HEAT_FROM_TORCH",
	"tempChange": 0.25,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(COLD_BIOME | SNOWY_BIOME | UPPER_BODY_IN_WATER | LOWER_BODY_IN_WATER)
& HEAT_FROM_TORCH
"""
}