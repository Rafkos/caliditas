{
	"factorName": "BAD_WEATHER",
	"tempChange": -0.85,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(IN_CLOSED_SPACE)
& (DRY_BIOME & STORM)
"""
}