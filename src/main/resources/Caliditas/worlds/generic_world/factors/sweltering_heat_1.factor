{
	"factorName": "SWELTERING_HEAT",
	"tempChange": 0.55,
	"tempUnit": "CELSIUS",
	"condition": 
"""
!(IN_CLOSED_SPACE | STORM)
& (DAY & DRY_BIOME)
"""
}