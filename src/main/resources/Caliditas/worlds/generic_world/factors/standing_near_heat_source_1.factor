{
	"factorName": "STANDING_NEAR_HEAT_SOURCE",
	"tempChange": 0.85,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(COLD_BIOME | SNOWY_BIOME | UPPER_BODY_IN_WATER | LOWER_BODY_IN_WATER)
& STANDING_NEAR_HEAT_SOURCE
"""
}