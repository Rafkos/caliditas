{
	"factorName": "UPPER_BODY_IN_WATER",
	"tempChange": -0.2,
	"tempUnit": "CELSIUS",
	"condition": 
"""
!(COLD_BIOME | SNOWY_BIOME | DRY_BIOME | WARM_BIOME)
& UPPER_BODY_IN_WATER
"""
}