{
	"factorName": "HEAT_FROM_GROUND",
	"tempChange": 0.75,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(UPPER_BODY_IN_WATER | LOWER_BODY_IN_WATER | UPPER_BODY_IN_WATER | COLD_BIOME | SNOWY_BIOME | DRY_BIOME)
& (LOW_ALTITUDE & IN_CLOSED_SPACE)
"""
}