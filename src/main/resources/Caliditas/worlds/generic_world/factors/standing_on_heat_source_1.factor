{
	"factorName": "STANDING_ON_HEAT_SOURCE",
	"tempChange": 1.2,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(COLD_BIOME | SNOWY_BIOME | UPPER_BODY_IN_WATER)
& STANDING_ON_HEAT_SOURCE
"""
}