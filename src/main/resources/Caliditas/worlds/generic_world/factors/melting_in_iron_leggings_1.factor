{
	"factorName": "MELTING_IN_IRON_LEGGINGS",
	"tempChange": 0.65,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(UNDER_ROOF | STORM)
& (WEARING_IRON_LEGGINGS & DRY_BIOME & DAY)
"""
}