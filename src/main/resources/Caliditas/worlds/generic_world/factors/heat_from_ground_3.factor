{
	"factorName": "HEAT_FROM_GROUND",
	"tempChange": 2.15,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(UPPER_BODY_IN_WATER | LOWER_BODY_IN_WATER)
& (LOW_ALTITUDE & IN_CLOSED_SPACE & SNOWY_BIOME)
"""
}