{
	"factorName": "CHILL_FROM_SHADOW",
	"tempChange": -0.65,
	"tempUnit": "CELSIUS",
	"condition":
"""
(WARM_BIOME & IN_CLOSED_SPACE & UNDER_ROOF)
"""
}