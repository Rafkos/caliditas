{
	"factorName": "BOOTS_PROTECTING_FROM_COLD",
	"tempChange": 0.35,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(LOWER_BODY_IN_WATER)
& (COLD_BIOME & WEARING_LEATHER_BOOTS)
"""
}