{
	"factorName": "CHESTPLATE_PROTECTING_FROM_COLD",
	"tempChange": 0.55,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(UPPER_BODY_IN_WATER)
& (COLD_BIOME & WEARING_LEATHER_CHESTPLATE)
"""
}