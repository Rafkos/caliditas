{
	"factorName": "NIGHT",
	"tempChange": -2.25,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(IN_CLOSED_SPACE | INFLUENCED_BY_ANY_HEAT_SOURCE)
& (MIDNIGHT & DRY_BIOME)
"""
}