{
	"factorName": "LEGGINGS_PROTECTING_FROM_COLD",
	"tempChange": 0.2,
	"tempUnit": "CELSIUS",
	"condition":
"""
!(LOWER_BODY_IN_WATER)
& (SNOWY_BIOME & WEARING_IRON_LEGGINGS)
"""
}