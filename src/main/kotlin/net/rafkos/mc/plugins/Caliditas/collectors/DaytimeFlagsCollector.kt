/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas.collectors

import net.rafkos.mc.plugins.Caliditas.Flag
import net.rafkos.mc.plugins.Caliditas.loaders.LoadersManagerHelper
import org.bukkit.World
import org.bukkit.entity.Player

class DaytimeFlagsCollector : FlagsCollector {
    override fun collectFlags(player: Player) {

        // time of day only matters in normal world
        if (player.world.environment == World.Environment.NORMAL) {
            val time = player.world.time
            val pd = LoadersManagerHelper.playerDataLoader.getPlayerData(player)
            when (time) {
                in 1000..5999 -> {
                    pd.addFlag(Flag("DAY", player.world))
                }
                in 6000..12999 -> {
                    pd.addFlag(Flag("NOON", player.world))
                }
                in 13000..17999 -> {
                    pd.addFlag(Flag("NIGHT", player.world))
                }
                else -> {
                    pd.addFlag(Flag("MIDNIGHT", player.world))
                }
            }
        }
    }
}