/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas.handlers

import net.rafkos.mc.plugins.Caliditas.Config
import net.rafkos.mc.plugins.Caliditas.MessagesGenerator
import net.rafkos.mc.plugins.Caliditas.PlayerData
import net.rafkos.mc.plugins.Caliditas.TemperatureState
import net.rafkos.mc.plugins.Caliditas.events.PlayerTemperatureStateChanged
import net.rafkos.mc.plugins.Caliditas.loaders.LoadersManagerHelper
import org.bukkit.Bukkit
import org.bukkit.entity.Player

/**
 * This is a Runnable class that handles online players' temperature effects (potion effects).
 *
 * @author thebadogre
 */
class EffectsHandler : Runnable {
    override fun run() {
        if (Config.stressTest) {
            for (i in 0..Config.stressTestPasses)
                execute()
        } else {
            execute()
        }
    }

    private fun execute() {
        if (!Config.pluginInstance.internalEnabled) {
            return
        }
        for (player in Bukkit.getServer().onlinePlayers) {
            handlePlayer(player)
        }
    }

    /**
     * This method assigns potion effects to player based on player's
     * current temperature state.
     *
     * @param player
     * @see net.rafkos.mc.plugins.Caliditas.TemperatureState
     */
    private fun handlePlayer(player: Player) {
        val newState = getTemperatureState(player)
        val pd: PlayerData = LoadersManagerHelper.playerDataLoader.getPlayerData(player)

        // check if state changed
        if (newState != pd.state) {
            // register event
            val event = PlayerTemperatureStateChanged(player, newState)
            Bukkit.getPluginManager().callEvent(event)
            if (!LoadersManagerHelper.getWorldConfigLoader(player.world).disableTemperatureStateChangeChatMessages)
                MessagesGenerator.stateStatus(player, newState)
        }

        // assign current state to player's last state
        pd.lastState = pd.state

        // assign new state to player's current state
        pd.state = newState

        // assign effects of given temperature state to player
        for (creator in Config.effectCreators) {
            creator.setTemperatureEffect(player, newState)
        }
    }

    /**
     * This method converts player's internal temperature to temperature state
     *
     * @param player - Player
     * @return returns player's temperature state
     */
    private fun getTemperatureState(player: Player): TemperatureState {
        // player's temperature
        val temperature: Double = LoadersManagerHelper.playerDataLoader.getPlayerData(player).temperature
        var state: TemperatureState = TemperatureState.MELTING
        val values = TemperatureState.values()
        for (i in TemperatureState.values().indices.reversed()) {
            if (temperature <= LoadersManagerHelper.getTemperatureStateLoader(player.world).states[values[i]]!!) {
                state = values[i]
            }
        }
        return state
    }
}