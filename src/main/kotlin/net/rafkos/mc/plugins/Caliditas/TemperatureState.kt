/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas

import net.rafkos.mc.plugins.Caliditas.loaders.LocaleManager.getLocale
import org.bukkit.ChatColor
import org.bukkit.command.CommandSender
import java.io.Serializable

/**
 * Represents temperature state of a player. It is used by
 * the EffectsHandler to assign temperature effects (potion effects).
 *
 * @author thebadogre
 * @see net.rafkos.mc.plugins.Caliditas.handlers.EffectsHandler
 */
enum class TemperatureState : Serializable {
    FREEZING, COLD, CHILL, NORMAL, WARM, HOT, MELTING;

    companion object {
        private val tempStateToChatColor = hashMapOf<TemperatureState, ChatColor>()

        init {
            tempStateToChatColor[FREEZING] = ChatColor.DARK_BLUE
            tempStateToChatColor[COLD] = ChatColor.BLUE
            tempStateToChatColor[CHILL] = ChatColor.DARK_AQUA
            tempStateToChatColor[NORMAL] = ChatColor.WHITE
            tempStateToChatColor[WARM] = ChatColor.YELLOW
            tempStateToChatColor[HOT] = ChatColor.GOLD
            tempStateToChatColor[MELTING] = ChatColor.RED
        }
    }

    val isAboveNormal: Boolean
        get() = !isBelowNormal && this != NORMAL

    val isBelowNormal: Boolean
        get() = this == FREEZING || this == COLD || this == CHILL

    val chatColor: ChatColor?
        get() = tempStateToChatColor[this]

    fun toString(executor: CommandSender?): String {
        return getLocale(this.toString(), executor!!)
    }
}