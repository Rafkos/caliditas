/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas.loaders

import net.rafkos.mc.plugins.Caliditas.Config
import org.bukkit.World

/**
 * A helper class for LoadersManager.
 *
 * @see LoadersManager
 */
object LoadersManagerHelper {
    fun getBiomesDataLoader(world: World): BiomesDataLoader {
        return LoadersManager.getLoaderForWorld(world, BiomesDataLoader::class.java) as BiomesDataLoader
    }

    fun getEffectsLoader(world: World): EffectsLoader {
        return LoadersManager.getLoaderForWorld(world, EffectsLoader::class.java) as EffectsLoader
    }

    fun getFactorsLoader(world: World): FactorsLoader {
        return LoadersManager.getLoaderForWorld(world, FactorsLoader::class.java) as FactorsLoader
    }

    fun getFlagsDurationLoader(world: World): FlagsDurationLoader {
        return LoadersManager.getLoaderForWorld(world, FlagsDurationLoader::class.java) as FlagsDurationLoader
    }

    fun getLocaleLoader(world: World): LocaleLoader {
        return LoadersManager.getLoaderForWorld(world, LocaleLoader::class.java) as LocaleLoader
    }

    val playerDataLoader: PlayerDataLoader = LoadersManager.playerDataLoader!!

    fun savePlayerDataLoader(): Boolean {
        return LoadersManager.playerDataLoader!!.save(Config.pluginInstance.dataFolder)
    }

    fun getHeatSourceLoader(world: World): HeatSourceLoader {
        return LoadersManager.getLoaderForWorld(world, HeatSourceLoader::class.java) as HeatSourceLoader
    }

    fun getWorldConfigLoader(world: World): WorldConfigLoader {
        return LoadersManager.getLoaderForWorld(world, WorldConfigLoader::class.java) as WorldConfigLoader
    }

    fun getTemperatureStateLoader(world: World): TemperatureStateLoader {
        return LoadersManager.getLoaderForWorld(world, TemperatureStateLoader::class.java) as TemperatureStateLoader
    }
}