/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas.loaders

import com.google.gson.JsonIOException
import com.google.gson.JsonSyntaxException
import net.rafkos.mc.plugins.Caliditas.Config
import java.io.File
import java.io.FileNotFoundException
import java.io.FileReader


class FlagsDurationLoader : Loader {
    /**
     * Contains info on how many ticks should the specified flag last.
     */
    val durationMap = hashMapOf<String, Int>()

    override fun load(dataFolder: File): Boolean {
        durationMap.clear()
        var success = false
        val flagsDurationFile = File(dataFolder.path, "flags_duration.json")
        try {
            val reader = FileReader(flagsDurationFile)
            val data = Config.gson.fromJson(reader, Array<ReceivedJson>::class.java)
            reader.close()
            for (fd in data) {
                require(validate(fd.flagDurationSeconds)) { "Incorrect values in flags_duration.json" }
                durationMap[fd.flagName] = fd.flagDurationSeconds * 20
            }
            success = true
        } catch (e: JsonSyntaxException) {
            Config.pluginInstance.logger.severe("Unable to load " + flagsDurationFile.path + "" +
                    " file due to incorrect JSON syntax.")
        } catch (e: JsonIOException) {
            Config.pluginInstance.logger.severe("Unable to load " + flagsDurationFile.path + "" +
                    " file due to incorrect JSON syntax.")
        } catch (e: FileNotFoundException) {
            Config.pluginInstance.logger.info("File " + flagsDurationFile.path + "" +
                    " does not exist.")
        }
        return success
    }

    private data class ReceivedJson(
            val flagName: String,
            val flagDurationSeconds: Int
    )

    companion object {
        private fun validate(durationSeconds: Int): Boolean {
            return durationSeconds >= 1
        }
    }
}