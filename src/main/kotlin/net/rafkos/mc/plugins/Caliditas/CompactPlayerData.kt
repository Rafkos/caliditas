/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas

import java.io.Serializable
import java.util.*

/**
 * This class is used only for serialization. It represents most important data
 * about player that needs to be stored after server shutdowns.
 *
 * @author thebadogre
 */
class CompactPlayerData(
        /**
         * Since Player class is not serializable I'm using UUID to identify player.
         */
        val uniqueId: UUID,

        /**
         * Store if player was immune to temperature due to /immune command (not by gamemodes)
         */
        val temperatureImmuneInternally: Boolean,

        /**
         * If player has enabled the temperature bar
         */
        val temperatureBarEnabled: Boolean,

        /**
         * A temperature unit preferred by player.
         *
         * @see net.rafkos.mc.plugins.Caliditas.Config.setupTemperatureConverters
         */
        val temperatureUnit: TemperatureUnit,

        /**
         * A last internal temperature of player
         */
        val temperature: Double,

        /**
         * A last temperature bar display mode.
         */
        val temperatureOnStatusBar: Boolean
) : Serializable {

    companion object {
        private const val serialVersionUID = 7226224771013054143L
    }
}