package net.rafkos.mc.plugins.Caliditas.listeners

import net.rafkos.mc.plugins.Caliditas.Flag
import net.rafkos.mc.plugins.Caliditas.loaders.LoadersManagerHelper
import org.bukkit.Material
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerItemConsumeEvent
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.PotionMeta

/**
 * The purpose of this listener is to collect flags based on consumed items. It will produce following flags
 * CONSUMED_{item type} and CONSUMED_FOOD_OR_DRINK.
 */
class ConsumablesListener : Listener {
    @EventHandler
    fun onItemConsumed(e: PlayerItemConsumeEvent) {
        // name of flag
        val flagName = collectFlagName(e.item)
        // use default duration if no other is specified
        val durationTicks: Int = LoadersManagerHelper.getFlagsDurationLoader(e.player.world).durationMap[flagName]
                ?: LoadersManagerHelper.getWorldConfigLoader(e.player.world).defaultConsumableFlagsDurationTicks
        // add flag
        LoadersManagerHelper.playerDataLoader.getPlayerData(e.player).also {
            it.addFlag(Flag(flagName, durationTicks))
            it.addFlag(Flag("CONSUMED_FOOD_OR_DRINK", e.player.world))
        }
    }

    companion object {
        private fun collectFlagName(item: ItemStack): String = when (item.type) {
            Material.POTION -> collectPotionFlagName(item.itemMeta!! as PotionMeta)
            else -> "CONSUMED_${item.type.toString().toUpperCase()}"
        }

        private fun collectPotionFlagName(meta: PotionMeta) =
                "CONSUMED_POTION_${meta.basePotionData.type.toString().toUpperCase()}"
    }
}