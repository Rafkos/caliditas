/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas

import net.rafkos.mc.plugins.Caliditas.collectors.*
import net.rafkos.mc.plugins.Caliditas.commands.CommandExecutorTemperature
import net.rafkos.mc.plugins.Caliditas.effects.GenericEffectCreator
import net.rafkos.mc.plugins.Caliditas.handlers.BurningFurnacesHandler
import net.rafkos.mc.plugins.Caliditas.handlers.EffectsHandler
import net.rafkos.mc.plugins.Caliditas.handlers.FactorsHandler
import net.rafkos.mc.plugins.Caliditas.handlers.MiscHandler
import net.rafkos.mc.plugins.Caliditas.listeners.*
import net.rafkos.mc.plugins.Caliditas.listeners.FireStockingListener.Companion.stopAllFireHandlers
import net.rafkos.mc.plugins.Caliditas.loaders.LoadersManager
import net.rafkos.mc.plugins.Caliditas.loaders.LoadersManagerHelper.playerDataLoader
import net.rafkos.mc.plugins.Caliditas.loaders.LoadersManagerHelper.savePlayerDataLoader
import org.bukkit.Bukkit
import org.bukkit.plugin.java.JavaPlugin

/**
 * Main class of Caliditas plugin.
 *
 * @author thebadogre
 */
class Caliditas : JavaPlugin() {
    /**
     * Tells the plugin whether or not it is enabled or disabled internally.
     * When set to false it will disable all gameplay functions of the plugin (commands will still work)
     */
    var internalEnabled = true

    var burningFurnacesListener: BurningFurnacesListener? = null

    override fun onEnable() {
        Config.pluginInstance = this

        // setup default the plugin's configuration
        if (Config.loadConfig()) {
            // adds default flag collectors references to the config so they can be seen and used by handlers
            Config.flagsCollectors = listOf(ClothesFlagsCollector(), HeatSourceFlagsCollector(), WeatherFlagsCollector(), DaytimeFlagsCollector(), PlayerActionsFlagsCollector(), PlayerSurroundingsFlagsCollector(), PotionEffectsFlagsCollector())

            // adds default effect creators to config for the same purpose as above
            Config.effectCreators = listOf(GenericEffectCreator())

            burningFurnacesListener = BurningFurnacesListener().also { it.initWorlds() }

            // creates tasks for handlers
            Bukkit.getScheduler().scheduleSyncRepeatingTask(this, FactorsHandler(), 1, 1)
            Bukkit.getScheduler().scheduleSyncRepeatingTask(this, EffectsHandler(), 1, 1)
            Bukkit.getScheduler().scheduleSyncRepeatingTask(this, MiscHandler(), 1, 1)
            Bukkit.getScheduler().scheduleSyncRepeatingTask(this, BurningFurnacesHandler(burningFurnacesListener!!), 1, 1)
            getCommand("cali")!!.setExecutor(CommandExecutorTemperature())
            server.pluginManager.registerEvents(TemperatureResetListener(), this)
            server.pluginManager.registerEvents(TemperatureBarDisableListener(), this)
            server.pluginManager.registerEvents(FireStockingListener(), this)
            server.pluginManager.registerEvents(WorldListener(), this)
            server.pluginManager.registerEvents(burningFurnacesListener!!, this)
            server.pluginManager.registerEvents(ConsumablesListener(), this)
            server.pluginManager.registerEvents(MiningBuildingListener(), this)
        }
    }



    override fun onDisable() {
        // remove temperature bars for all online players
        for (p in server.onlinePlayers) {
            TemperatureBar.removeTemperatureBar(playerDataLoader.getPlayerData(p!!))
        }

        burningFurnacesListener?.extinguishAllFakes()

        // save player data
        savePlayerDataLoader()

        //remove fire maintainers
        stopAllFireHandlers()
        LoadersManager.unloadAll()
    }
}