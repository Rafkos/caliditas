package net.rafkos.mc.plugins.Caliditas.commands

import net.rafkos.mc.plugins.Caliditas.Config
import net.rafkos.mc.plugins.Caliditas.loaders.LocaleManager
import org.bukkit.command.CommandSender
import org.bukkit.permissions.Permission
import java.util.*

class ReloadCommand : PluginCommand() {
    override fun execute(executor: CommandSender, args: LinkedList<String>): Boolean {
        if (args.isEmpty()) {
            Config.pluginInstance.onDisable()
            Config.pluginInstance.onEnable()
            executor.sendMessage(LocaleManager.getLocale("CMD_CALI_RELOADED", executor))
            return true
        }
        return false
    }

    override fun getPermissions(): Array<Permission> {
        return arrayOf(permission, adminPermission)
    }

    companion object {
        val permission = Permission("caliditas.reload")
    }
}