/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas.loaders

import com.google.gson.JsonIOException
import com.google.gson.JsonSyntaxException
import net.rafkos.mc.plugins.Caliditas.Config
import java.io.File
import java.io.FileNotFoundException
import java.io.FileReader

class BannedWorldsLoader : Loader {
    override fun load(dataFolder: File): Boolean {
        bannedWorlds.clear()
        var success = false
        val bannedWorldsFile = File(dataFolder.path, "banned_worlds.json")
        try {
            val data = Config.gson.fromJson(FileReader(bannedWorldsFile), Array<ReceivedJson>::class.java)
            for (bw in data) {
                bannedWorlds.add(bw.worldName)
            }
            success = true
        } catch (e: JsonSyntaxException) {
            Config.pluginInstance.logger.severe("Unable to load " + bannedWorldsFile.path + "" +
                    " file due to incorrect JSON syntax.")
        } catch (e: JsonIOException) {
            Config.pluginInstance.logger.severe("Unable to load " + bannedWorldsFile.path + "" +
                    " file due to incorrect JSON syntax.")
        } catch (e: FileNotFoundException) {
            Config.pluginInstance.logger.info("File " + bannedWorldsFile.path + "" +
                    " does not exist.")
        }
        return success
    }

    private class ReceivedJson(val worldName: String)

    companion object {
        var bannedWorlds = hashSetOf<String>()
    }
}