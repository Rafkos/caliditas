/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas.commands

import net.rafkos.mc.plugins.Caliditas.loaders.LoadersManagerHelper
import net.rafkos.mc.plugins.Caliditas.loaders.LocaleManager
import org.bukkit.Bukkit
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import org.bukkit.permissions.Permission
import java.util.*

class ImmuneCommand : PluginCommand() {
    override fun execute(executor: CommandSender, args: LinkedList<String>): Boolean {
        if (args.size == 1 && executor is Player) {
            return changeImmuneStatus(executor, executor, args.poll())
        } else if (args.size == 2 && (executor.hasPermission(permissionOtherImmune) || executor.hasPermission(adminPermission))) {
            val player = Bukkit.getServer().getPlayer(args.poll())
            if (player != null) {
                return changeImmuneStatus(player, executor, args.poll())
            }
        }
        return false
    }

    private fun changeImmuneStatus(player: Player, executor: CommandSender, arg: String): Boolean {
        val pd = LoadersManagerHelper.playerDataLoader.getPlayerData(player)
        val flag: Boolean = try {
            java.lang.Boolean.parseBoolean(arg)
        } catch (e: Exception) {
            executor.sendMessage(LocaleManager.getLocale("CMD_SYNTAX_ERROR", executor))
            return false
        }
        if (flag) {
            executor.sendMessage(LocaleManager.getLocale("CMD_IMMUNE_ENABLED", executor, player.name))
        } else {
            executor.sendMessage(LocaleManager.getLocale("CMD_IMMUNE_DISABLED", executor, player.name))
        }
        pd.temperatureImmuneInternally = flag
        return true
    }

    override fun getPermissions(): Array<Permission> {
        return arrayOf(permission, adminPermission)
    }

    companion object {
        val permission = Permission("caliditas.immune")
        val permissionOtherImmune = Permission("caliditas.immune.other")
    }
}