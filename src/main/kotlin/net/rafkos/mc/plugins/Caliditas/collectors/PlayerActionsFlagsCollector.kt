/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas.collectors

import net.rafkos.mc.plugins.Caliditas.Flag
import net.rafkos.mc.plugins.Caliditas.PlayerData
import net.rafkos.mc.plugins.Caliditas.loaders.LoadersManagerHelper
import org.bukkit.Material
import org.bukkit.entity.Player

/**
 * Collects flags from player actions (e.g. swimming, sprinting).
 *
 * @author thebadogre
 */
class PlayerActionsFlagsCollector : FlagsCollector {
    override fun collectFlags(player: Player) {
        val pd = LoadersManagerHelper.playerDataLoader.getPlayerData(player)

        // check player actions in water
        collectPlayerWaterFunFlags(player, pd)
        if (player.isSwimming) {
            pd.addFlag(Flag("SWIMMING", player.world))
        } else if (player.isSprinting) {
            pd.addFlag(Flag("SPRINTING", player.world))
        } else if (player.isSneaking) {
            pd.addFlag(Flag("SNEAKING", player.world))
        } else if (player.isSleeping) {
            pd.addFlag(Flag("SLEEPING", player.world))
        } else if (isWalking(player, pd)) {
            pd.addFlag(Flag("WALKING", player.world))
        } else if (player.isFlying || player.isGliding) {
            pd.addFlag(Flag("FLYING", player.world))
        }
    }

    private fun isWalking(player: Player, pd: PlayerData): Boolean {
        return if (playerHasMoved(player, pd)) {
            // if player is not in the air and moved that means he walked (he's not jumping or flying)
            player.location.y == player.location.block.y.toDouble()
        } else false
    }

    private fun playerHasMoved(player: Player, pd: PlayerData): Boolean {
        val currentLocation = player.location
        var flag = false
        // check if player has history
        if (pd.lastLocation != null) {
            // a move can only happen when the last step was in the same world environment
            if (currentLocation.world!!.environment == pd.lastLocation!!.world!!.environment) {
                // if location is different that means player moved
                if (currentLocation.x != pd.lastLocation!!.x || currentLocation.z != pd.lastLocation!!.z || currentLocation.y != pd.lastLocation!!.y) {
                    flag = true
                }
            }
        }

        // store for next iteration
        pd.lastLocation = currentLocation
        return flag
    }

    private fun collectPlayerWaterFunFlags(player: Player, pd: PlayerData) {
        val playerLowerBodyBlock = player.location.block
        val playerUpperBodyBlock = player.eyeLocation.block

        // check if upper body is in water
        if (playerUpperBodyBlock.blockData.material == Material.WATER) {
            pd.addFlag(Flag("UPPER_BODY_IN_WATER", player.world))
        }
        // check if lower body is in water
        if (playerLowerBodyBlock.blockData.material == Material.WATER) {
            pd.addFlag(Flag("LOWER_BODY_IN_WATER", player.world))
            if (!player.isSwimming) {
                // if block below player is water then obviously he is floating
                if (playerLowerBodyBlock.getRelative(0, -1, 0).blockData.material == Material.WATER) {
                    pd.addFlag(Flag("FLOATING_IN_WATER", player.world))
                } else if (player.location.y > player.location.block.y) {
                    pd.addFlag(Flag("FLOATING_IN_WATER", player.world))
                }
            }
        }
    }
}