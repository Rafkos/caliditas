package net.rafkos.mc.plugins.Caliditas.commands

import net.rafkos.mc.plugins.Caliditas.loaders.LoadersManagerHelper
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import org.bukkit.permissions.Permission
import java.util.*

class BarModeCommand : PluginCommand() {
    override fun execute(executor: CommandSender, args: LinkedList<String>): Boolean {
        return if (args.size == 1 && executor is Player) {
            changeTemperatureDisplay(executor, args.poll())
        } else false
    }

    private fun changeTemperatureDisplay(player: Player, mode: String): Boolean {
        val pd = LoadersManagerHelper.playerDataLoader.getPlayerData(player)
        if (mode.equals("status", ignoreCase = true)) {
            pd.temperatureOnStatusBar = false
            return true
        } else if (mode.equals("temperature", ignoreCase = true)) {
            pd.temperatureOnStatusBar = true
            return true
        }
        return false
    }

    override fun getPermissions(): Array<Permission> {
        return arrayOf(permission, userPermission, adminPermission)
    }

    companion object {
        val permission = Permission("caliditas.barmode")
    }
}