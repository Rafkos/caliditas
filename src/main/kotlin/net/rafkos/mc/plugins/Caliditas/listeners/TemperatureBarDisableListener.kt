/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas.listeners

import net.rafkos.mc.plugins.Caliditas.TemperatureBar
import net.rafkos.mc.plugins.Caliditas.loaders.LoadersManagerHelper.playerDataLoader
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerChangedWorldEvent
import org.bukkit.event.player.PlayerQuitEvent

class TemperatureBarDisableListener : Listener {
    @EventHandler
    fun onPlayerChangedWorldEvent(e: PlayerChangedWorldEvent) {
        nullTemperatureBarRef(e.player)
    }

    @EventHandler
    fun onPlayerDisconnected(e: PlayerQuitEvent) {
        nullTemperatureBarRef(e.player)
    }

    companion object {
        private fun nullTemperatureBarRef(p: Player) {
            TemperatureBar.removeTemperatureBar(playerDataLoader.getPlayerData(p))
        }
    }
}