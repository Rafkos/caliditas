/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas.commands

import net.rafkos.mc.plugins.Caliditas.Config
import net.rafkos.mc.plugins.Caliditas.loaders.LocaleManager
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import org.bukkit.permissions.Permission
import java.util.*

/**
 * Main command of the plugin.
 *
 * @author thebadogre
 */
class RootCommand : PluginCommand() {
    private val barCommand: PluginCommand = BarCommand()
    private val immuneCommand: PluginCommand = ImmuneCommand()
    private val statusCommand: PluginCommand = StatusCommand()
    private val pluginModeCommand: PluginCommand = ModeCommand()
    private val flagsCommand: PluginCommand = FlagsCommand()
    private val unitCommand: PluginCommand = UnitCommand()
    private val reloadCommand: PluginCommand = ReloadCommand()
    private val barModeCommand: PluginCommand = BarModeCommand()
    private val permission = Permission("caliditas.info")

    override fun execute(executor: CommandSender, args: LinkedList<String>): Boolean {
        if (!Config.pluginInstance.internalEnabled && !(executor.hasPermission(ModeCommand.permission) || executor.hasPermission(adminPermission))) {
            executor.sendMessage(LocaleManager.getLocale("CMD_CALI_IS_DISABLED", executor))
            return true
        } else if (args.size == 0) {
            executor.sendMessage(LocaleManager.getLocale("CMD_AVAILABLE_COMMANDS", executor))
            if (executor.hasPermission(ModeCommand.permission) || executor.hasPermission(adminPermission)) {
                executor.sendMessage(LocaleManager.getLocale("CMD_CALI_ENABLE", executor))
            }
            if (executor is Player && (executor.hasPermission(UnitCommand.permission) || executor.hasPermission(adminPermission) || executor.hasPermission(userPermission))) {
                executor.sendMessage(LocaleManager.getLocale("CMD_CALI_UNIT_INFO", executor, Config.temperatureConverters.keys))
            }
            if (executor is Player && (executor.hasPermission(StatusCommand.permission) || executor.hasPermission(adminPermission) || executor.hasPermission(userPermission))) {
                executor.sendMessage(LocaleManager.getLocale("CMD_CALI_STATUS_INFO", executor))
            }
            if (executor.hasPermission(StatusCommand.permissionCheckOther) || executor.hasPermission(adminPermission)) {
                executor.sendMessage(LocaleManager.getLocale("CMD_CALI_STATUS_INFO_OTHER", executor))
            }
            if (executor is Player && (executor.hasPermission(ImmuneCommand.permission) || executor.hasPermission(adminPermission))) {
                executor.sendMessage(LocaleManager.getLocale("CMD_CALI_IMMUNE", executor))
            }
            if (executor.hasPermission(ImmuneCommand.permissionOtherImmune) || executor.hasPermission(adminPermission)) {
                executor.sendMessage(LocaleManager.getLocale("CMD_CALI_IMMUNE_OTHER", executor))
            }
            if (executor is Player && (executor.hasPermission(BarCommand.permission) || executor.hasPermission(adminPermission) || executor.hasPermission(userPermission))) {
                executor.sendMessage(LocaleManager.getLocale("CMD_CALI_BAR", executor))
            }
            if (executor is Player && (executor.hasPermission(BarModeCommand.permission) || executor.hasPermission(adminPermission) || executor.hasPermission(userPermission))) {
                executor.sendMessage(LocaleManager.getLocale("CMD_BARMODE_INFO", executor))
            }
            if (executor.hasPermission(BarCommand.permissionOtherBar) || executor.hasPermission(adminPermission)) {
                executor.sendMessage(LocaleManager.getLocale("CMD_CALI_BAR_OTHER", executor))
            }
            if (executor is Player && (executor.hasPermission(FlagsCommand.permission) || executor.hasPermission(adminPermission))) {
                executor.sendMessage(LocaleManager.getLocale("CMD_FLAGS_DEBUG", executor))
            }
            if (executor.hasPermission(ReloadCommand.permission)) {
                executor.sendMessage(LocaleManager.getLocale("CMD_CALI_RELOAD_INFO", executor))
            }
            return true
        } else {
            val subCommand = args.poll()
            var success = false
            if (subCommand.equals("status", ignoreCase = true)) {
                if (statusCommand.hasPermissions(executor)) {
                    success = statusCommand.execute(executor, args)
                }
            } else if (subCommand.equals("immune", ignoreCase = true)) {
                if (immuneCommand.hasPermissions(executor)) {
                    success = immuneCommand.execute(executor, args)
                }
            } else if (subCommand.equals("bar", ignoreCase = true)) {
                if (barCommand.hasPermissions(executor)) {
                    success = barCommand.execute(executor, args)
                }
            } else if (subCommand.equals("enable", ignoreCase = true)) {
                if (pluginModeCommand.hasPermissions(executor)) {
                    success = pluginModeCommand.execute(executor, args)
                }
            } else if (subCommand.equals("flags", ignoreCase = true)) {
                if (flagsCommand.hasPermissions(executor)) {
                    success = flagsCommand.execute(executor, args)
                }
            } else if (subCommand.equals("unit", ignoreCase = true)) {
                if (unitCommand.hasPermissions(executor)) {
                    success = unitCommand.execute(executor, args)
                }
            } else if (subCommand.equals("reload", ignoreCase = true)) {
                if (reloadCommand.hasPermissions(executor)) {
                    success = reloadCommand.execute(executor, args)
                }
            } else if (subCommand.equals("barmode", ignoreCase = true)) {
                if (barModeCommand.hasPermissions(executor)) {
                    success = barModeCommand.execute(executor, args)
                }
            }
            if (success) return true
        }
        executor.sendMessage(LocaleManager.getLocale("CMD_SYNTAX_ERROR", executor))
        return false
    }

    override fun getPermissions(): Array<Permission> {
        return arrayOf(permission, userPermission, adminPermission)
    }
}