/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas.loaders

import com.google.gson.JsonIOException
import com.google.gson.JsonSyntaxException
import net.rafkos.mc.plugins.Caliditas.Config
import net.rafkos.mc.plugins.Caliditas.TemperatureState
import net.rafkos.mc.plugins.Caliditas.TemperatureUnit
import java.io.File
import java.io.FileNotFoundException
import java.io.FileReader

class TemperatureStateLoader : Loader {
    val states = hashMapOf<TemperatureState, Double>()

    override fun load(dataFolder: File): Boolean {
        states.clear()
        var success = false
        val statesFile = File(dataFolder, "temp_states.json")
        try {
            val states = Config.gson.fromJson(FileReader(statesFile), Array<ReceivedJson>::class.java)
            require(valid(states)) { "Incorrect values in temp_states.json" }
            for (j in states) {
                val threshold = Config.temperatureConverters[j.tempUnit]!!.convertToMachine(j.tempThreshold)
                this.states[j.tempState] = threshold
            }
            success = true
        } catch (e: JsonSyntaxException) {
            Config.pluginInstance.logger.severe("Unable to load " + statesFile.path + "" +
                    " file due to incorrect JSON syntax.")
        } catch (e: JsonIOException) {
            Config.pluginInstance.logger.severe("Unable to load " + statesFile.path + "" +
                    " file due to incorrect JSON syntax.")
        } catch (e: FileNotFoundException) {
            Config.pluginInstance.logger.info("File " + statesFile.path + "" +
                    " does not exist.")
        }
        return success
    }

    private fun valid(states: Array<ReceivedJson>): Boolean {
        val temp = hashMapOf<TemperatureState, Double>()
        for (j in states) {
            val threshold = Config.temperatureConverters[j.tempUnit]!!.convertToMachine(j.tempThreshold)
            if (threshold > Config.maxTemperature || threshold < Config.minTemperature) {
                return false
            }
            temp[j.tempState] = threshold
        }
        return temp[TemperatureState.MELTING]!! > temp[TemperatureState.HOT]!! && temp[TemperatureState.HOT]!! > temp[TemperatureState.WARM]!! && temp[TemperatureState.WARM]!! > temp[TemperatureState.NORMAL]!! && temp[TemperatureState.NORMAL]!! > temp[TemperatureState.CHILL]!! && temp[TemperatureState.CHILL]!! > temp[TemperatureState.COLD]!! && temp[TemperatureState.COLD]!! > temp[TemperatureState.FREEZING]!!
    }

    private inner class ReceivedJson(
            val tempState: TemperatureState,
            val tempThreshold: Double,
            val tempUnit: TemperatureUnit)

}