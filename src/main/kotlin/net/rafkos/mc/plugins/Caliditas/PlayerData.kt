/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas

import net.rafkos.mc.plugins.Caliditas.commands.PluginCommand
import net.rafkos.mc.plugins.Caliditas.events.PlayerFlagAddedEvent
import net.rafkos.mc.plugins.Caliditas.loaders.BannedWorldsLoader
import net.rafkos.mc.plugins.Caliditas.loaders.LoadersManagerHelper.getTemperatureStateLoader
import org.bukkit.Bukkit
import org.bukkit.GameMode
import org.bukkit.Location
import java.io.Serializable
import java.util.*

/**
 * This class represents player's data. It contains information about player's
 * temperature, player's last temperature, step, target temperature etc.
 * It also contains a set of current flags of a player. There is also
 * an information if player is immune to the temperature or if he has
 * enabled a temperature bar.
 *
 * @author thebadogre
 * @see net.rafkos.mc.plugins.Caliditas.handlers.FactorsHandler
 */
class PlayerData(
        /**
         * Player is identified by his UUID (object Player is not serializable).
         */
        val uniqueId: UUID,

        /**
         * Determines in what unit should the temperature be displayed to the player.
         *
         * @see net.rafkos.mc.plugins.Caliditas.Config.temperatureConverters
         * @see net.rafkos.mc.plugins.Caliditas.converters.TemperatureValueConverter
         * @see net.rafkos.mc.plugins.Caliditas.MessagesGenerator.targetTemperature
         */
        var temperatureUnit: TemperatureUnit,

        /**
         * Stores a location of the player from previous tick.
         */
        var lastLocation: Location?,

        /**
         * Stores a reference to the player's temperature bar (boss bar).
         *
         * @see net.rafkos.mc.plugins.Caliditas.TemperatureBar
         */
        var temperatureBar: TemperatureBar?
) : Serializable {

    /**
     * Current temperature of the player calculated by FactorsHandler
     *
     * @see net.rafkos.mc.plugins.Caliditas.handlers.FactorsHandler
     */
    var temperature = 0.0

    /**
     * Makes player immune to all the effects from the temperature,
     * disables temperature bar and temperature change. It can only be set by
     * a command.
     *
     * @see net.rafkos.mc.plugins.Caliditas.commands.ImmuneCommand
     *
     * @see net.rafkos.mc.plugins.Caliditas.TemperatureBar.updateTemperatureBar
     */
    var temperatureImmuneInternally = false

    /**
     * Represents a temperature step from previous iteration (tick) of FactorsHandler.
     * It is used for displaying player's status.
     *
     * @see net.rafkos.mc.plugins.Caliditas.MessagesGenerator.temperatureChange
     * @see net.rafkos.mc.plugins.Caliditas.handlers.FactorsHandler
     */
    var lastStep = 0.0

    /**
     * It serves a similar purpose as lastStep.
     *
     * @see PlayerData.lastStep
     */
    var lastTemperature = 0.0

    /**
     * It serves a similar purpose as lastStep.
     *
     * @see PlayerData.lastStep
     */
    var lastTemperatureChange = 0.0

    /**
     * It serves a similar purpose as lastStep.
     *
     * @see PlayerData.lastStep
     */
    var lastTargetTemperature = 0.0

    /**
     * Current state of the player calculated by EffectsHandler.
     *
     * @see net.rafkos.mc.plugins.Caliditas.handlers.EffectsHandler
     */
    var state: TemperatureState = TemperatureState.NORMAL

    /**
     * It serves a similar purpose as lastStep.
     *
     * @see PlayerData.lastStep
     */
    var lastState = TemperatureState.NORMAL

    /**
     * Indicates if temperature bar should be displayed.
     *
     * @see net.rafkos.mc.plugins.Caliditas.commands.BarCommand
     * @see net.rafkos.mc.plugins.Caliditas.TemperatureBar.updateTemperatureBar
     */
    var temperatureBarEnabled = true

    /**
     * Stores current flags of a player. They're being collected by flags collectors
     * each tick in order to be used by FactorHandler for calculating factors.
     *
     * @see net.rafkos.mc.plugins.Caliditas.handlers.FactorsHandler
     * @see net.rafkos.mc.plugins.Caliditas.collectors.FlagsCollector
     */
    val flags = hashSetOf<Flag>()

    /**
     * Stores flags of a player from previous iteration.
     *
     * @see PlayerData.flags
     */
    var lastFlags = setOf<Flag>()

    /**
     * Stores a list of factors from previous iteration. It is used similarly
     * to lastStep - to generate messages.
     *
     * @see PlayerData.lastStep
     */
    var lastFactors = listOf<Factor>()

    var temperatureOnStatusBar = false

    /**
     * Adds the flag to the player. Calls event and restores flag duration (if flag was present before).
     *
     * @param flag - Flag
     */
    fun addFlag(flag: Flag) {
        val event = PlayerFlagAddedEvent(Bukkit.getPlayer(uniqueId)!!, flag)
        Bukkit.getPluginManager().callEvent(event)
        if (!event.cancelled) {
            flags.remove(flag)
            flags.add(flag)
        }
    }

    /**
     * Similar to getLastStep but instead of giving last tick step it gives an
     * approximate step from the last second. It simply multiplies last step
     * by 20.0 (20 ticks = 1 second).
     *
     * @return returns an approximate temperature step from last second.
     */
    val lastSecondStep: Double
        get() = lastStep * 20.0

    /**
     * Sets current player's temperature to match NORMAL state.
     *
     * @see net.rafkos.mc.plugins.Caliditas.TemperatureState
     */
    fun resetTemperature() {
        temperature = getTemperatureStateLoader(
                Bukkit.getPlayer(uniqueId)!!.world).states[TemperatureState.NORMAL]!!
    }// player has no permissions for the plugin features

    /**
     * First the method checks if player is immune to the temperature
     * internally - that means if he was made immune by a command - or if he has no permissions
     * for the feature. If not then player immunity depends on his game mode
     * - if he's in creative or spectator mode he's also immune. If world is
     * banned from the temperature then the player is also immune.
     * The method is used by various classes
     * (e.g. temperature bar classes, handlers, message generators and commands).
     * It can also be used for offline players.
     *
     * @return Returns true if player is immune to the temperature.
     * @see net.rafkos.mc.plugins.Caliditas.handlers.FactorsHandler
     *
     * @see net.rafkos.mc.plugins.Caliditas.TemperatureBar
     *
     * @see net.rafkos.mc.plugins.Caliditas.commands
     */
    val isTemperatureImmune: Boolean
        get() = if (temperatureImmuneInternally) {
            true
        } else {
            val player = Bukkit.getServer().getPlayer(uniqueId)
            if (player != null) {
                // player has no permissions for the plugin features
                if (!player.hasPermission("caliditas.temperature") && !player.hasPermission(PluginCommand.adminPermission) && !player.hasPermission(PluginCommand.userPermission) && !player.isOp) {
                    true
                } else player.isDead || player.gameMode == GameMode.CREATIVE || player.gameMode == GameMode.SPECTATOR || BannedWorldsLoader.bannedWorlds.contains(player.world.name)
            } else {
                false
            }
        }

    companion object {
        private const val serialVersionUID = 5248252368179086654L
    }
}