/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas.converters

class FahrenheitValueConverter : TemperatureValueConverter {
    override fun convertToHuman(value: Double): Double {
        return value * multiplierToHuman + offsetToHuman
    }

    override val symbol: String
        get() = "°F"

    override fun convertToMachine(value: Double): Double {
        return (value - offsetToHuman) / multiplierToHuman
    }

    // 1*F = -17.22*C
    // 10*F = -12.22*C
    override val multiplierToHuman: Double
        get() = 0.4

    override val offsetToHuman: Double
        get() = 78.6
}