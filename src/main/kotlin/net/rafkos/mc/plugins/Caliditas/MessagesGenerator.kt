/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas

import misc.TableGenerator
import net.rafkos.mc.plugins.Caliditas.Config.temperatureConverters
import net.rafkos.mc.plugins.Caliditas.loaders.LoadersManagerHelper.getWorldConfigLoader
import net.rafkos.mc.plugins.Caliditas.loaders.LoadersManagerHelper.playerDataLoader
import net.rafkos.mc.plugins.Caliditas.loaders.LocaleManager.getLocale
import org.bukkit.Bukkit
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import java.util.*
import kotlin.math.roundToInt

/**
 * This class contains various methods for sending messages to players/server.
 *
 * @author thebadogre
 */
object MessagesGenerator {
    /**
     * Sends a message about player's current state to command sender.
     *
     * @param player   - Player whose temperature state is being displayed.
     * @param executor - Executor is basically a command sender.
     */
    fun temperatureState(player: Player, executor: CommandSender) {
        val temperatureState = playerDataLoader.getPlayerData(player).state
        val stateText = getLocale(temperatureState.toString(), executor)

        // give temperature state some color
        val sb = StringBuilder(Objects.requireNonNull(temperatureState.chatColor).toString())
        sb.append(stateText)

        // assume that player executed the command for himself
        if (player === executor) {
            sendMessageToRightReceiver(player, getLocale("MSG_CURRENT_STATE", executor, sb.toString()))
        } else {
            sendMessageToRightReceiver(executor, getLocale("MSG_CURRENT_STATE_OTHER", executor, player.name, sb.toString()))
        }
    }

    /**
     * Sends a message about player's target temperature to command sender.
     *
     * @param player   - Player whose target temperature is being displayed
     * @param executor - Executor is basically a command sender.
     */
    fun targetTemperature(player: Player, executor: CommandSender) {
        val pd = playerDataLoader.getPlayerData(player)
        val unit = (if (executor is Player) pd.temperatureUnit else getWorldConfigLoader(player.world).defaultTemperatureUnit)
        val temperature = pd.lastTargetTemperature
        val temperatureHuman = temperatureConverters[unit]!!.convertToHuman(temperature)
        if (player === executor) {
            sendMessageToRightReceiver(executor, getLocale("MSG_TARGET_TEMP", executor, ((temperatureHuman * 100.0).roundToInt() / 100.0).toString() + temperatureConverters[unit]!!.symbol))
        } else {
            sendMessageToRightReceiver(executor, getLocale("MSG_TARGET_TEMP_OTHER", executor, player.name, ((temperatureHuman * 100.0).roundToInt() / 100.0).toString() + temperatureConverters[unit]!!.symbol))
        }
    }

    /**
     * Sends a message about player's temperature change to command sender.
     *
     * @param player   - Player whose temperature change is being displayed
     * @param executor - Executor is basically a command sender.
     */
    fun temperatureChange(player: Player, executor: CommandSender) {
        val pd = playerDataLoader.getPlayerData(player)
        val unit = (if (executor is Player) pd.temperatureUnit else getWorldConfigLoader(player.world).defaultTemperatureUnit)
        val temperatureStepPerSec = pd.lastSecondStep
        if (player === executor) {
            sendMessageToRightReceiver(executor, getLocale("MSG_TEMP_CHANGE", executor, colorDouble(temperatureStepPerSec * temperatureConverters[unit]!!.multiplierToHuman) + temperatureConverters[unit]!!.symbol))
        } else {
            sendMessageToRightReceiver(executor, getLocale("MSG_TEMP_CHANGE_OTHER", executor, player.name, colorDouble(temperatureStepPerSec * temperatureConverters[unit]!!.multiplierToHuman) + temperatureConverters[unit]!!.symbol))
        }
    }

    /**
     * Sends a message about player's current temperature to command sender.
     *
     * @param player   - Player whose current temperature is being displayed
     * @param executor - Executor is basically a command sender.
     */
    fun currentTemperature(player: Player, executor: CommandSender) {
        val pd = playerDataLoader.getPlayerData(player)
        val unit = (if (executor is Player) pd.temperatureUnit else getWorldConfigLoader(player.world).defaultTemperatureUnit)
        val temperature = pd.lastTemperature
        val temperatureHuman = temperatureConverters[unit]!!.convertToHuman(temperature)
        if (player === executor) {
            sendMessageToRightReceiver(executor, getLocale("MSG_CURRENT_TEMP", executor, ((temperatureHuman * 100.0).roundToInt() / 100.0).toString() + temperatureConverters[unit]!!.symbol))
        } else {
            sendMessageToRightReceiver(executor, getLocale("MSG_CURRENT_STATE_OTHER", executor, player.name, ((temperatureHuman * 100.0).roundToInt() / 100.0).toString() + temperatureConverters[unit]!!.symbol))
        }
    }

    /**
     * Sends a message about player's factors to command sender.
     * They will be displayed as a table.
     *
     * @param player   - Player whose factors are being displayed
     * @param executor - Executor is basically a command sender.
     */
    fun factorsTable(player: Player?, executor: CommandSender) {
        val pd = playerDataLoader.getPlayerData(player!!)
        val unit = if (executor is Player) pd.temperatureUnit else pd.temperatureUnit

        // setup table
        val tg = TableGenerator(TableGenerator.Alignment.LEFT, TableGenerator.Alignment.CENTER)

        // add rows to the table
        for (f in pd.lastFactors) {
            val factor = (colorDouble(f.tempChange
                    * temperatureConverters[unit]!!.multiplierToHuman)
                    + temperatureConverters[unit]!!.symbol)
            val sb = StringBuilder("§f")
            sb.append(getLocale(f.factorName, executor))
            tg.addRow(sb.toString(), factor)
        }
        val receiver = if (executor is Player) TableGenerator.Receiver.CLIENT else TableGenerator.Receiver.CONSOLE
        for (line in tg.generate(receiver, true, true)) {
            sendMessageToRightReceiver(executor, line)
        }
    }

    /**
     * This method creates a string from double and adds a color
     * to it based on +/- sign of the value.
     *
     * @param d - Double value.
     * @return Returns a string made of color + double.
     */
    private fun colorDouble(d: Double): String {
        val sb = StringBuilder()
        if (d > 0) {
            sb.append("§e+")
        } else if (d < 0) {
            sb.append("§b")
        }
        sb.append((d * 100.0).roundToInt() / 100.0)
        return sb.toString()
    }

    /**
     * Sends a message to player about his temperature state change.
     *
     * @param player - Player whose state changed.
     * @param state  - New state.
     */
    fun stateStatus(player: Player, state: TemperatureState) {
        // give temperature state some color
        val sb = StringBuilder(Objects.requireNonNull(state.chatColor).toString())
        val stateText = getLocale(state.toString(), player)
        sb.append(stateText)
        sendMessageToRightReceiver(player, getLocale("MSG_STATE_CHANGED", player, sb.toString()))
    }

    /**
     * Sends a message about player's immune status to command sender.
     *
     * @param player   - Player whose immunity status is being checked.
     * @param executor - Executor is basically a command sender.
     */
    fun immuneMessage(player: Player, executor: CommandSender) {

        // assume that player executed the command for himself
        if (player === executor) {
            sendMessageToRightReceiver(player, getLocale("MSG_IMMUNE", player))
        } else {
            sendMessageToRightReceiver(executor, getLocale("MSG_IMMUNE_OTHER", executor, player.name))
        }
    }

    /**
     * Determines who the command sender is and sends him a message.
     *
     * @param sender  - Command sender.
     * @param message - Message.
     */
    private fun sendMessageToRightReceiver(sender: CommandSender, message: String) {
        if (sender is Player) {
            sender.sendMessage(message)
        } else {
            Bukkit.getConsoleSender().sendMessage(message)
        }
    }
}