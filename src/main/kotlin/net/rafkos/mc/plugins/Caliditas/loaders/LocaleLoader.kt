/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas.loaders

import com.google.gson.JsonIOException
import com.google.gson.JsonSyntaxException
import net.rafkos.mc.plugins.Caliditas.Config
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.InputStreamReader
import java.text.MessageFormat
import java.util.*

/**
 * Simple and useful class for managing localizations.
 *
 * @author thebadogre
 */
class LocaleLoader : Loader {
    private val locale = hashMapOf<String, HashMap<String, String>>()

    /**
     * A simple function for obtaining localized strings from string ID,
     * command sender's language and arguments.
     *
     * @param name   - String ID.
     * @param sender - To who the message is addressed.
     * @param args   - Arguments to be inserted into the localized string (if any).
     * @return - Returns localized text (based on sender's language) with arguments inserted.
     */
    fun getLocale(name: String, sender: CommandSender, vararg args: Any?): String {
        var locale = DEFAULT_LANG
        if (sender is Player) {
            locale = sender.locale
        }
        return getLocale(name, locale, *args)
    }

    /**
     * A simple function for obtaining localized strings from string ID,
     * language code and arguments.
     *
     * @param name - String ID.
     * @param lang - Language code, e.g. en_us.
     * @param args - Arguments to be inserted into the localized string (if any).
     * @return - Returns localized text with arguments inserted.
     */
    fun getLocale(name: String, lang: String, vararg args: Any?): String {
        // check if NAME exists in localized strings
        if (locale.containsKey(name)) {
            // check if localization exists for given language
            if (locale[name]!!.containsKey(lang)) {
                return MessageFormat.format(locale[name]!![lang] ?: "LOC_ERR", *args)
            } else if (locale[name]!!.containsKey(DEFAULT_LANG)) {
                return MessageFormat.format(locale[name]!![DEFAULT_LANG] ?: "LOC_ERR", *args)
            }
        }

        // return string ID if no localization
        return name
    }

    override fun load(dataFolder: File): Boolean {
        locale.clear()
        if (dataFolder.listFiles() == null) return false
        for (f in dataFolder.listFiles()!!) {
            if (f.isFile) {
                val fileName = f.nameWithoutExtension
                if (fileName.startsWith("locale_") && f.extension == "json") {
                    if (!loadLocale(fileName.substring(7), f)) {
                        return false
                    }
                }
            }
        }
        return true
    }

    private fun loadLocale(lang: String, source: File): Boolean {
        var success = false
        try {
            val reader = InputStreamReader(FileInputStream(source), Charsets.UTF_8)
            val entries = Config.gson.fromJson(reader, Array<LocalizedEntry>::class.java)
            reader.close()
            for (entry in entries) {
                if (!locale.containsKey(entry.name)) {
                    locale[entry.name] = hashMapOf()
                }
                locale[entry.name]!![lang] = entry.text
            }
            success = true
        } catch (e: JsonSyntaxException) {
            Config.pluginInstance.logger.severe("Unable to load " + source.path + "" +
                    " file due to incorrect JSON syntax.")
        } catch (e: JsonIOException) {
            Config.pluginInstance.logger.severe("Unable to load " + source.path + "" +
                    " file due to incorrect JSON syntax.")
        } catch (e: FileNotFoundException) {
            Config.pluginInstance.logger.info("File " + source.path + "" +
                    " does not exist.")
        }
        return success
    }

    private data class LocalizedEntry(
            val name: String,
            val text: String)

    companion object {
        const val DEFAULT_LANG = "en_us"
    }
}