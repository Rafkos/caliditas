/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas.loaders

import com.google.gson.JsonIOException
import com.google.gson.JsonSyntaxException
import net.rafkos.mc.plugins.Caliditas.Config
import net.rafkos.mc.plugins.Caliditas.Factor
import net.rafkos.mc.plugins.Caliditas.TemperatureUnit
import net.rafkos.utils.booltree.Parser
import net.rafkos.utils.extjson.ExtJson
import java.io.File
import java.io.FileReader
import java.io.IOException

class FactorsLoader : Loader {
    val factors = hashSetOf<Factor>()
    fun load(f: Factor): Boolean {
        return factors.add(f)
    }

    override fun load(dataFolder: File): Boolean {
        factors.clear()
        val factorsFolder = File(dataFolder.path, "/factors/")
        if (factorsFolder.listFiles() == null) {
            Config.pluginInstance.logger.info("Unable to load factors for " + dataFolder.name +
                    ". Folder does not exist.")
            return false
        }
        var success = true
        var loadedFactors = 0
        var incorrectFactors = 0
        for (factorConfig in factorsFolder.listFiles()!!) {
            if (factorConfig.isFile && factorConfig.extension == "factor") {
                try {
                    val r = FileReader(factorConfig.absolutePath)
                    val f = Config.gson.fromJson(ExtJson.toJson(r.readText()), ReceivedJson::class.java)
                    r.close()
                    if (!valid(f)) {
                        Config.pluginInstance.logger.severe(
                                "Incorrect values in \"" + factorConfig.name + "\".")
                        success = false
                        break
                    }
                    val machineTemperatureChange: Double = f.tempChange / Config.temperatureConverters[f.tempUnit]!!.multiplierToHuman
                    val factor = Factor(f.factorName, machineTemperatureChange, Parser.parseTree(f.condition))
                    factors.add(factor)
                    loadedFactors++
                } catch (e: IOException) {
                    Config.pluginInstance.logger.severe("Unable to load " + factorConfig.path + "" +
                            " file due to unspecified IO exception.")
                    incorrectFactors++
                    success = false
                    break
                } catch (e: JsonSyntaxException) {
                    Config.pluginInstance.logger.severe("Unable to load " + factorConfig.path + "" +
                            " file due to incorrect exJSON syntax.")
                    incorrectFactors++
                    success = false
                    break
                } catch (e: JsonIOException) {
                    Config.pluginInstance.logger.severe("Unable to load " + factorConfig.path + "" +
                            " file due to incorrect exJSON syntax.")
                    incorrectFactors++
                    success = false
                    break
                }
            } else {
                Config.pluginInstance.logger.warning("Found incorrect file \"" +
                        factorConfig.name + "\" inside \"" + factorsFolder.path +
                        "\" folder. This file will be skipped.")
            }
        }
        Config.pluginInstance.logger.info("Loaded " + loadedFactors +
                " factors with " + incorrectFactors + " errors for world \"" + dataFolder.name + "\".")
        return success
    }

    private fun valid(f: ReceivedJson): Boolean {
        return Config.temperatureConverters.containsKey(f.tempUnit)
    }

    class ReceivedJson(
            val factorName: String,
            val tempChange: Double,
            val tempUnit: TemperatureUnit,
            val condition: String)
}