/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas.loaders

import net.rafkos.mc.plugins.Caliditas.CompactPlayerData
import net.rafkos.mc.plugins.Caliditas.Config
import net.rafkos.mc.plugins.Caliditas.PlayerData
import net.rafkos.mc.plugins.Caliditas.TemperatureBar
import org.bukkit.entity.Player
import java.io.*
import java.util.*

class PlayerDataLoader : Loader {
    private val uniqueIdToPlayerData = hashMapOf<UUID, PlayerData>()

    fun getPlayerData(player: Player): PlayerData {
        return if (uniqueIdToPlayerData.containsKey(player.uniqueId)) {
            uniqueIdToPlayerData[player.uniqueId]!!
        } else {
            addPlayer(player)
            uniqueIdToPlayerData[player.uniqueId]!!
        }
    }

    fun getAll(): List<PlayerData> = uniqueIdToPlayerData.values.toList()

    private fun addPlayer(player: Player): Boolean {
        return if (!uniqueIdToPlayerData.containsKey(player.uniqueId)) {
            val pd = PlayerData(
                    player.uniqueId,
                    LoadersManagerHelper.getWorldConfigLoader(player.world).defaultTemperatureUnit,
                    player.location,
                    TemperatureBar(player))
            pd.temperature = Config.baseTemperature
            pd.temperatureImmuneInternally = false
            uniqueIdToPlayerData[player.uniqueId] = pd
            true
        } else {
            false
        }
    }

    override fun load(dataFolder: File): Boolean {
        uniqueIdToPlayerData.clear()
        val playerDataFile = File(dataFolder, "/player_data.dat")
        if (!playerDataFile.exists()) {
            return true
        }
        try {
            val input = ObjectInputStream(FileInputStream(playerDataFile))
            try {
                val cpdList = input.readObject() as ArrayList<*>
                for (cpd in cpdList) {
                    cpd as CompactPlayerData
                    val pd = PlayerData(
                            cpd.uniqueId,
                            cpd.temperatureUnit,
                            null,
                            null
                    )
                    pd.temperature = cpd.temperature
                    pd.temperatureImmuneInternally = cpd.temperatureImmuneInternally
                    pd.temperatureBarEnabled = cpd.temperatureBarEnabled
                    pd.temperatureUnit = cpd.temperatureUnit
                    pd.temperatureOnStatusBar = cpd.temperatureOnStatusBar
                    uniqueIdToPlayerData[pd.uniqueId] = pd
                }
            } catch (e: ClassNotFoundException) {
                Config.pluginInstance.logger.warning("Wrong format of players data.")
            }
        } catch (e: IOException) {
            Config.pluginInstance.logger.warning("Could not load players data.")
        }
        return true
    }

    fun save(dataFolder: File): Boolean {
        var success = false
        if (!dataFolder.exists()) {
            dataFolder.mkdirs()
        }
        val playerDataFile = File(dataFolder, "/player_data.dat")
        try {
            val out = ObjectOutputStream(FileOutputStream(playerDataFile))
            val cpdList = ArrayList<CompactPlayerData>()
            for (pd in uniqueIdToPlayerData.values) {
                val cpd = CompactPlayerData(pd.uniqueId, pd.temperatureImmuneInternally, pd.temperatureBarEnabled, pd.temperatureUnit, pd.temperature, pd.temperatureOnStatusBar)
                cpdList.add(cpd)
            }
            out.writeObject(cpdList)
            success = true
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return success
    }
}