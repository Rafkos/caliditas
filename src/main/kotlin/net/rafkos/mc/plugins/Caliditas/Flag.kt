/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas

import net.rafkos.mc.plugins.Caliditas.loaders.LoadersManagerHelper
import org.bukkit.World

/**
 * This class represents a temperature flag. Flags are used to calculate factors.
 */
class Flag(val flagName: String, var durationTicks: Int) {
    constructor(flagName: String, world: World) :
            this(flagName, obtainBaseFlagDuration(flagName, world))

    override fun equals(other: Any?): Boolean {
        if (other !is Flag) return false
        return flagName == other.flagName
    }

    override fun toString(): String = when {
        durationTicks > 20 -> "$flagName (${((durationTicks * 100 / 20).toDouble() / 100.0)} s.)"
        else -> flagName
    }

    override fun hashCode(): Int {
        return flagName.hashCode()
    }

    fun clone(): Flag = Flag(flagName, durationTicks)

    companion object {
        /**
         * Returns base duration of specific flag in provided world.
         */
        private fun obtainBaseFlagDuration(flagName: String, world: World): Int = LoadersManagerHelper.getFlagsDurationLoader(world).durationMap[flagName]
                ?: 1
    }
}