package net.rafkos.mc.plugins.Caliditas.listeners

import net.rafkos.mc.plugins.Caliditas.Config
import net.rafkos.mc.plugins.Caliditas.loaders.LoadersManagerHelper
import org.bukkit.*
import org.bukkit.block.Furnace
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockBreakEvent
import org.bukkit.event.block.BlockPlaceEvent
import org.bukkit.event.inventory.FurnaceBurnEvent
import org.bukkit.event.inventory.FurnaceSmeltEvent
import org.bukkit.event.inventory.InventoryClickEvent
import org.bukkit.event.world.ChunkLoadEvent
import org.bukkit.inventory.FurnaceInventory

/**
 * "Burning furnaces" feature makes furnaces burn for an extended time
 * when specific fuel material is provided. No melting can be done with
 * such material.
 *
 * @author thebadogre
 */
class BurningFurnacesListener : Listener {
    /**
     * Contains location of furnaces burning because of the feature mechanics.
     */
    private val fakeBurning = hashSetOf<Location>()

    /**
     * Contains location of all furnaces that are burning or smelting a material (including fake ones).
     */
    private val genericBurning = hashSetOf<Location>()

    val furnacesToCheck = mutableListOf<FurnaceCheckSchedule>()

    fun isFurnaceBurning(location: Location): Boolean = genericBurning.contains(location)

    @EventHandler(ignoreCancelled = true)
    fun onChunkLoaded(e: ChunkLoadEvent) {
        registerAllFurnacesInChunk(e.chunk)
    }

    /**
     * Init all furnaces on the server (only use on startup).
     */
    fun initWorlds() = Bukkit.getWorlds().forEach { initWorld(it) }

    fun initWorld(world: World) = world.loadedChunks.forEach { registerAllFurnacesInChunk(it) }

    private fun registerAllFurnacesInChunk(c: Chunk) {
        for (tileEntity in c.tileEntities) {
            if (tileEntity is Furnace) {
                scheduleCheck(tileEntity.inventory, 1)
            }
        }
    }

    @EventHandler
    fun onBlockDestroyed(e: BlockBreakEvent) {
        if (e.block.blockData.material == Material.FURNACE) {
            fakeBurning.remove(e.block.location)
            genericBurning.remove(e.block.location)
        }
    }

    @EventHandler
    fun onBlockCreated(e: BlockPlaceEvent) {
        if (e.block.blockData.material == Material.FURNACE) {
            val state = e.block.state
            if (state is Furnace)
                scheduleCheck(state.inventory, 1)
        }
    }

    @EventHandler
    fun onInventoryClick(e: InventoryClickEvent) {
        val inv = e.inventory
        if (inv is FurnaceInventory)
            scheduleCheck(inv, 1)
    }

    @EventHandler
    fun onFurnaceSmeltEvent(e: FurnaceSmeltEvent) {
        val furnace = e.block.state
        if (furnace !is Furnace)
            return
        val burnTimeLeft = furnace.burnTime
        scheduleCheck(furnace.inventory, burnTimeLeft.toLong() + 1)
    }

    @EventHandler
    fun onFurnaceBurnEvent(e: FurnaceBurnEvent) {
        val furnace = e.block.state
        if (furnace !is Furnace)
            return
        val burnTimeLeft = furnace.burnTime
        scheduleCheck(furnace.inventory, burnTimeLeft.toLong() + 1)
    }

    private fun scheduleCheck(inv: FurnaceInventory, delay: Long) {
        val scheduledCheck = FurnaceCheckSchedule(inv.location!!, delay)
        furnacesToCheck.add(scheduledCheck)
    }

    fun handle(inv: FurnaceInventory) {
        val furnace = inv.holder ?: return
        val featureEnabled = isFeatureEnabled(inv.location!!.world ?: return)
        if (featureEnabled && furnaceIsEmptyNothingIsSmeltingAndFuelMaterialProvided(furnace)) {
            igniteFake(furnace)
            scheduleCheck(furnace.inventory, defaultBurnTime + 1.toLong())
        } else if (featureEnabled && wasFakeBurningButJustCompletedAndHasNoSmeltingMaterial(furnace, inv)) {
            igniteFake(furnace)
            scheduleCheck(furnace.inventory, defaultBurnTime + 1.toLong())
        } else if (featureEnabled && wasFakeBurningButProcessWasInterruptedBySmeltingItem(furnace, inv)) {
            extinguishFake(furnace)
            scheduleCheck(furnace.inventory, 1.toLong())
        } else if (furnaceHasSmeltingMaterialAndFuelAndIsNotBurning(furnace)) {
            scheduleCheck(furnace.inventory, 1.toLong())
        } else if (furnaceIsBurning(furnace) && !genericBurning.contains(furnace.location)) {
            scheduleCheck(furnace.inventory, furnace.burnTime + 1.toLong())
            genericBurning.add(furnace.location)
        } else if (wasBurningButJustCompletedAndHasNoSmeltingMaterial(furnace)) {
            genericBurning.remove(furnace.location)
        }
        if (!featureEnabled && fakeBurning.contains(furnace.location)) {
            extinguishFake(furnace)
        }
    }

    private fun igniteFake(furnace: Furnace) {
        furnace.burnTime = defaultBurnTime
        furnace.update()
        genericBurning.add(furnace.location)
        fakeBurning.add(furnace.location)
    }

    private fun extinguishFake(furnace: Furnace) {
        furnace.burnTime = 0
        furnace.update()
        val location = furnace.location
        genericBurning.remove(location)
        fakeBurning.remove(location)
    }

    fun exstinguishFakesInWorld(world: World) {
        val it = fakeBurning.iterator()
        while (it.hasNext()) {
            val location = it.next()
            if (location.world?.uid == world.uid) {
                genericBurning.remove(location)
                it.remove()
            }
            if (location.block.blockData.material == Material.FURNACE) {
                val furnace = location.block.state as Furnace
                furnace.burnTime = 0
                furnace.update()
            }
        }
    }

    fun extinguishAllFakes() {
        val it = fakeBurning.iterator()
        while (it.hasNext()) {
            val location = it.next()
            if (location.block.blockData.material == Material.FURNACE) {
                val furnace = location.block.state as Furnace
                furnace.burnTime = 0
                furnace.update()
            }
            genericBurning.remove(location)
            it.remove()
        }
    }

    private fun isFeatureEnabled(world: World?): Boolean {
        var enabled = true
        if (world == null)
            enabled = false
        else if (!LoadersManagerHelper.getWorldConfigLoader(world).featureBurningFurnacesEnabled ||
                !Config.pluginInstance.internalEnabled)
            enabled = false
        if (!enabled) {
            extinguishAllFakes()
        }
        return enabled
    }

    private fun wasFakeBurningButJustCompletedAndHasNoSmeltingMaterial(furnace: Furnace, inv: FurnaceInventory) =
            fakeBurning.contains(furnace.location) && inv.smelting == null && furnace.burnTime == 0.toShort()

    private fun wasFakeBurningButProcessWasInterruptedBySmeltingItem(furnace: Furnace, inv: FurnaceInventory) =
            fakeBurning.contains(furnace.location) && inv.smelting != null

    private fun furnaceIsEmptyNothingIsSmeltingAndFuelMaterialProvided(furnace: Furnace) =
            furnaceHasFuel(furnace.inventory) && furnace.burnTime == 0.toShort() && furnace.inventory.smelting == null

    private fun furnaceHasSmeltingMaterialAndFuelAndIsNotBurning(furnace: Furnace) =
            furnaceHasFuel(furnace.inventory) && furnace.burnTime == 0.toShort() && furnace.inventory.smelting != null

    private fun furnaceIsBurning(furnace: Furnace) = furnace.burnTime > 0.toShort()

    private fun wasBurningButJustCompletedAndHasNoSmeltingMaterial(furnace: Furnace) =
        genericBurning.contains(furnace.location) && furnace.burnTime == 0.toShort()

    data class FurnaceCheckSchedule(val location: Location, var ticksTillCheck: Long)

    companion object {
        private const val defaultBurnTime = 2000.toShort()
        private var fuelMaterials = setOf(Material.STICK, Material.BOWL)

        fun furnaceHasFuel(inventory: FurnaceInventory): Boolean {
            val fuel = inventory.fuel ?: return false
            return isMaterialFuel(fuel.type)
        }

        fun isMaterialFuel(fuel: Material): Boolean {
            return if (fuelMaterials.contains(fuel)) {
                true
            } else fuel.isBurnable || fuel.isFuel
        }
    }
}