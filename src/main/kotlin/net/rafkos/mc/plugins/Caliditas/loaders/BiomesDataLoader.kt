/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas.loaders

import com.google.gson.JsonIOException
import com.google.gson.JsonSyntaxException
import net.rafkos.mc.plugins.Caliditas.BiomeData
import net.rafkos.mc.plugins.Caliditas.Config
import net.rafkos.mc.plugins.Caliditas.Flag
import net.rafkos.mc.plugins.Caliditas.TemperatureUnit
import org.bukkit.block.Biome
import java.io.File
import java.io.FileNotFoundException
import java.io.FileReader

class BiomesDataLoader : Loader {
    val biomeData = hashMapOf<String, BiomeData>()

    override fun load(dataFolder: File): Boolean {
        biomeData.clear()
        var success = false
        val biomesFile = File(dataFolder.path, "biomes.json")
        try {
            val biomes = Config.gson.fromJson(FileReader(biomesFile), Array<ReceivedJson>::class.java)
            for (b in biomes) {
                require(valid(b)) { "Incorrect values in biomes.json" }
                val machineTemperature = Config.temperatureConverters[b.tempUnit]!!.convertToMachine(b.targetBodyTemp)
                val flags = hashSetOf<Flag>()
                b.flags.forEach { flags.add(Flag(it, 1)) }
                val bt = BiomeData(b.biomeName, machineTemperature, flags)
                biomeData[bt.biomeName] = bt
            }
            possibleBiomes.filter { !biomeData.containsKey(it) }.forEach {
                Config.pluginInstance.logger.warning("No biome data specified for biome \"${it}\". " +
                        "Please add new biome to biomes.json.") }

            biomeData.filter { !possibleBiomes.contains(it.key) }.forEach {
                Config.pluginInstance.logger.warning("Incorrect biome data found: \"${it.key}\". " +
                        "Please remove it from biomes.json.")}

            success = true
        } catch (e: JsonSyntaxException) {
            Config.pluginInstance.logger.severe("Unable to load " + biomesFile.path + "" +
                    " file due to incorrect JSON syntax.")
        } catch (e: JsonIOException) {
            Config.pluginInstance.logger.severe("Unable to load " + biomesFile.path + "" +
                    " file due to incorrect JSON syntax.")
        } catch (e: FileNotFoundException) {
            Config.pluginInstance.logger.info("File " + biomesFile.path + "" +
                    " does not exist.")
        }

        return success
    }

    private fun valid(b: ReceivedJson): Boolean {
        if (!Config.temperatureConverters.containsKey(b.tempUnit)) {
            return false
        }
        val machineTemperature = Config.temperatureConverters[b.tempUnit]!!.convertToMachine(b.targetBodyTemp)
        return machineTemperature <= Config.maxTemperature && machineTemperature >= Config.minTemperature
    }

    private class ReceivedJson(
            val biomeName: String,
            val targetBodyTemp: Double,
            val tempUnit: TemperatureUnit,
            val flags: Array<String>)

    companion object {
        val possibleBiomes = hashSetOf<String>()
        init { Biome.values().iterator().forEach { possibleBiomes.add(it.name) }
                .also { possibleBiomes.add("GENERIC") } }
    }
}