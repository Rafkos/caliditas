/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas

import com.google.gson.Gson
import net.rafkos.mc.plugins.Caliditas.collectors.FlagsCollector
import net.rafkos.mc.plugins.Caliditas.converters.CelsiusValueConverter
import net.rafkos.mc.plugins.Caliditas.converters.FahrenheitValueConverter
import net.rafkos.mc.plugins.Caliditas.converters.TemperatureValueConverter
import net.rafkos.mc.plugins.Caliditas.effects.EffectCreator
import net.rafkos.mc.plugins.Caliditas.loaders.BannedWorldsLoader
import net.rafkos.mc.plugins.Caliditas.loaders.LoadersManager
import net.rafkos.mc.plugins.Caliditas.loaders.LocaleManager.loadGenericLocale
import java.io.File
import java.io.IOException
import java.net.URLDecoder
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*
import java.util.zip.ZipEntry
import java.util.zip.ZipFile

/**
 * This class contains default configuration of the plugin.
 *
 * @author thebadogre
 */
object Config {
    /**
     * SET FALSE FOR RELEASE BUILD!!!
     */
    const val stressTest = false
    const val stressTestPasses = 100

    lateinit var pluginInstance: Caliditas

    /**
     * Json parser used in the plugin
     */
    val gson = Gson()

    /**
     * This hashMapOf contains strings of temperature units
     * (e.g. celsius, fahrenheit) and corresponding converters
     *
     * @see net.rafkos.mc.plugins.Caliditas.converters.TemperatureValueConverter
     */
    val temperatureConverters = hashMapOf<TemperatureUnit, TemperatureValueConverter>()

    /**
     * Variables of internal temperature
     */
    const val baseTemperature = 50.0
    const val maxTemperature = 100.0
    const val minTemperature = 0.0
    const val maxHeatSourceRadius = 12

    /**
     * List of flags collectors
     *
     * @see net.rafkos.mc.plugins.Caliditas.collectors.FlagsCollector
     */
    var flagsCollectors: List<FlagsCollector> = mutableListOf()

    /**
     * List of effect creators
     *
     * @see net.rafkos.mc.plugins.Caliditas.effects.EffectCreator
     */
    var effectCreators: List<EffectCreator> = mutableListOf()

    /**
     * Loads config of the plugin
     *
     * @return flag that represents success or failure of loading the config
     */
    fun loadConfig(): Boolean {

        // ensure config files are present in plugins folder
        try {
            copyConfigFilesFromJarIfMissing()
        } catch (e: IOException) {
            e.printStackTrace()
            return false
        }
        setupTemperatureConverters()
        if (!LoadersManager.loadAllWorlds()) {
            return false
        }
        return if (!BannedWorldsLoader().load(pluginInstance.dataFolder)) {
            false
        } else loadGenericLocale()
    }

    /**
     * Classes implementing the TemperatureValueConverter interface need to be
     * added in this method.
     *
     * @see net.rafkos.mc.plugins.Caliditas.converters.TemperatureValueConverter
     */
    private fun setupTemperatureConverters() {
        temperatureConverters[TemperatureUnit.CELSIUS] = CelsiusValueConverter()
        temperatureConverters[TemperatureUnit.FAHRENHEIT] = FahrenheitValueConverter()
    }

    /**
     * This method will copy all the Caliditas config from jar of the plugin if any is missing.
     */
    private fun copyConfigFilesFromJarIfMissing() {

        // Caliditas data folder
        val dataFolder = File(pluginInstance.dataFolder.absolutePath)

        if (dataFolder.exists()) {
            pluginInstance.logger.info(
                    "Configuration folder found. " +
                            "If you want a fresh one please delete plugins/Caliditas and restart the server.")
            return
        }

        // ensure Caliditas data folder exists
        dataFolder.mkdir()
        File(dataFolder, "help/").mkdirs()
        File(dataFolder, "worlds/generic_world/").mkdirs()
        File(dataFolder, "worlds/generic_world/factors").mkdirs()

        // URL to jar file
        val jar = Caliditas::class.java.protectionDomain.codeSource.location
        val zf = ZipFile(URLDecoder.decode(jar.path, StandardCharsets.UTF_8.toString()))
        val entries = zf.entries() as Enumeration<*>
        while (entries.hasMoreElements()) {
            val e = entries.nextElement() as ZipEntry
            val entry = e.name

            // all config files are present in jar!/Caliditas/ folder
            if (entry.startsWith("Caliditas/")) {
                // a name of a config file from jar
                val name = entry.substring(10)

                // a path to the Caliditas data folder + name of the file
                val local = File(dataFolder.absolutePath + "/" + name)
                // check if the file exists in Caliditas data folder
                if (!local.exists()) {
                    // if not then copy it from the jar
                    val input = zf.getInputStream(e)
                    Files.copy(input, Paths.get(local.absolutePath))
                    input.close()
                }
            }
        }
        zf.close()
    }
}