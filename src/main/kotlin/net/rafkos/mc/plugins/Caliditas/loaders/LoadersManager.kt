/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas.loaders

import net.rafkos.mc.plugins.Caliditas.Config
import net.rafkos.mc.plugins.Caliditas.loaders.FlagsDurationLoader
import net.rafkos.mc.plugins.Caliditas.loaders.HeatSourceLoader
import net.rafkos.mc.plugins.Caliditas.loaders.LocaleLoader
import org.bukkit.Bukkit
import org.bukkit.World
import java.io.File
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean
import java.util.function.Consumer

/**
 * Purpose of this class is to manage loading data for given worlds.
 */
object LoadersManager {
    var worldToLoadersMap = hashMapOf<String, MutableMap<Class<out Loader>, Loader>>()
    var playerDataLoader: PlayerDataLoader? = null

    private val LOADERS = arrayOf(
            FlagsDurationLoader::class.java,
            BiomesDataLoader::class.java,
            EffectsLoader::class.java,
            FactorsLoader::class.java,
            WorldConfigLoader::class.java,
            HeatSourceLoader::class.java,
            LocaleLoader::class.java,
            TemperatureStateLoader::class.java)

    /**
     * Loads data for all world on this server. Loads generic data.
     *
     * @return Returns false if errors occurred.
     */
    fun loadAllWorlds(): Boolean {
        worldToLoadersMap.clear()
        if (!loadWorld("generic_world")) {
            Config.pluginInstance.logger.severe("Generic data folder does not exist.")
            return false
        }
        playerDataLoader = PlayerDataLoader()
        playerDataLoader!!.load(Config.pluginInstance.dataFolder)
        val success = AtomicBoolean(true)
        Bukkit.getWorlds().forEach(Consumer { world: World ->
            if (!loadWorld(world.name)) {
                success.set(false)
            }
        })
        return success.get()
    }

    /**
     * @param worldName World name for which the data will be loaded. If no world specific data is found then it will use generic config.
     * @return Returns true if world has been loaded correctly or no data was found (world uses generic files).
     * Returns false when errors have occurred.
     */
    fun loadWorld(worldName: String): Boolean {
        val genericDataFolder = File(Config.pluginInstance.dataFolder, "worlds/generic_world")
        val dataFolder = File(Config.pluginInstance.dataFolder, "worlds/$worldName")
        return if (genericDataFolder.exists() && genericDataFolder.isDirectory) {
            val loaders = createLoaders()
            if (loaders == null) {
                Config.pluginInstance.logger.severe("An error has occurred while processing one of the loaders. " + "The loading process is aborted.")
                return false
            }
            val verifiedLoaders = hashMapOf<Class<out Loader>, Loader>()
            for (l in loaders.values) {
                if (!l.load(dataFolder)) {
                    if (genericDataFolder != dataFolder) {
                        Config.pluginInstance.logger.info("World specific data not found, using generic data for:" +
                                " " + l.javaClass.simpleName + ".")
                        verifiedLoaders[l.javaClass] = worldToLoadersMap["generic_world"]!![l.javaClass]!!
                    } else {
                        Config.pluginInstance.logger.severe("World specific data not found, tried to use generic data but failed miserably. " +
                                "Is data missing in Caliditas/worlds/generic_world folder?")
                        return false
                    }
                } else {
                    verifiedLoaders[l.javaClass] = l
                }
            }
            worldToLoadersMap[worldName] = verifiedLoaders
            true
        } else {
            Config.pluginInstance.logger.severe("Generic data folder does not exist.")
            false
        }
    }

    /**
     * Creates loader for specified data folder.
     *
     * @return Returns map of loaders of given class. Returns null if any of the loaders was unable to start.
     */
    private fun createLoaders(): HashMap<Class<out Loader>, Loader>? {
        val mapOfLoaders = hashMapOf<Class<out Loader>, Loader>()
        for (lClass in LOADERS) {
            var l: Loader
            l = try {
                lClass.getConstructor().newInstance()
            } catch (e: Exception) {
                e.printStackTrace()
                return null
            }
            mapOfLoaders[lClass] = l
        }
        return mapOfLoaders
    }

    /**
     * Removes data of specified world.
     *
     * @param worldName World name.
     * @return Returns true if unloaded correctly.
     */
    fun unloadWorld(worldName: String): Boolean {
        if (worldToLoadersMap.containsKey(worldName)) {
            for (l in worldToLoadersMap[worldName]!!.values) {
                if (l is PlayerDataLoader) {
                    val dataFolder = File(Config.pluginInstance.dataFolder, "worlds/$worldName")
                    l.save(dataFolder)
                }
            }
            worldToLoadersMap.remove(worldName)
            return true
        }
        return false
    }

    /**
     * Removes all loaded worlds' data.
     */
    fun unloadAll() {
        HashSet(worldToLoadersMap.keys).forEach { unloadWorld(it) }
    }

    /**
     * Returns loader of given class for specified world.
     *
     * @param world       World instance.
     * @param loaderClass Class of loader.
     * @return Returns null if not found.
     */
    fun getLoaderForWorld(world: World?, loaderClass: Class<*>?): Loader? {
        var l: Loader? = null
        if (world != null && worldToLoadersMap.containsKey(world.name)) {
            l = worldToLoadersMap[world.name]!![loaderClass]
        }
        return l ?: worldToLoadersMap["generic_world"]!![loaderClass]
    }

    /**
     * @param lClass Loader class.
     * @return Returns a list of all loaders of given class from all worlds.
     */
    fun getAllLoadersOfClass(lClass: Class<out Loader>): List<Loader> {
        val loaders: MutableList<Loader> = mutableListOf()
        worldToLoadersMap.values.forEach(Consumer { loadersMap: MutableMap<Class<out Loader>, Loader> ->
            val l = loadersMap[lClass]
            if (l != null) {
                loaders.add(l)
            }
        })
        return loaders
    }
}