/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas

import net.rafkos.mc.plugins.Caliditas.Config.temperatureConverters
import net.rafkos.mc.plugins.Caliditas.loaders.LoadersManagerHelper.playerDataLoader
import net.rafkos.mc.plugins.Caliditas.loaders.LocaleManager.getLocale
import org.bukkit.Bukkit
import org.bukkit.boss.BarColor
import org.bukkit.boss.BarStyle
import org.bukkit.boss.BossBar
import org.bukkit.entity.Player
import kotlin.math.roundToInt

class TemperatureBar(private val player: Player) {
    private lateinit var bossBar: BossBar

    /**
     * Adds bossbar to player.
     */
    private fun init() {
        val bb = Bukkit.createBossBar("", BarColor.BLUE, BarStyle.SOLID)
        bb.addPlayer(player)
        bossBar = bb
    }

    /**
     * This function updates temperature bar's info.
     */
    private fun updateTemperatureBarInfo() {
        val pd = playerDataLoader.getPlayerData(player)
        bossBar.progress = calculateProgress(pd.temperature)
        val builder = StringBuilder(pd.state.chatColor.toString())

        // switch between status and temperature display
        if (!pd.temperatureOnStatusBar) {
            builder.append(pd.state.toString(player))
        } else {
            val unit = pd.temperatureUnit
            val convertedTemperature = temperatureConverters[unit]!!.convertToHuman(pd.temperature)
            builder.append((convertedTemperature * 100.0).roundToInt() / 100.0)
            builder.append(temperatureConverters[unit]!!.symbol)
        }
        bossBar.setTitle(getLocale("TEMPERATURE", player, builder.toString()))
        when {
            pd.state.isBelowNormal -> {
                bossBar.color = BarColor.BLUE
            }
            pd.state.isAboveNormal -> {
                bossBar.color = BarColor.YELLOW
            }
            else -> {
                bossBar.color = BarColor.WHITE
            }
        }
    }

    private fun calculateProgress(temperature: Double): Double {
        return (temperature - Config.minTemperature) / (Config.maxTemperature - Config.minTemperature)
    }

    private fun remove() {
        bossBar.removeAll()
    }

    companion object {
        /**
         * Creates a temperature bar for player if one does not exist.
         *
         * @param p  - Player whose temperature bar is being added.
         * @param pd - Player's data.
         */
        private fun createTemperatureBar(p: Player, pd: PlayerData) {
            if (pd.temperatureBar != null) {
                removeTemperatureBar(pd)
            }
            pd.temperatureBar = TemperatureBar(p)
            updateTemperatureBar(p)
        }

        /**
         * This method updates existing temperature bar STATUS.
         * It determines if the temperature bar should be removed, updated or created.
         *
         * @param p - Player
         */
        fun updateTemperatureBar(p: Player) {
            val pd = playerDataLoader.getPlayerData(p)

            // check if plugin is enabled
            if (pd.temperatureBar != null && !Config.pluginInstance.internalEnabled) {
                removeTemperatureBar(pd)
            } else if (pd.temperatureBarEnabled && !pd.isTemperatureImmune) {
                // create temperature bar if enabled but the object does not exist
                if (pd.temperatureBar == null) {
                    createTemperatureBar(p, pd)
                } else {
                    // update temperature bar info
                    pd.temperatureBar!!.updateTemperatureBarInfo()
                }
            } else {
                // remove if exists
                if (pd.temperatureBar != null) {
                    removeTemperatureBar(pd)
                }
            }
        }

        /**
         * Removes temperature bar from player.
         *
         * @param pd - Player's data.
         */
        fun removeTemperatureBar(pd: PlayerData) {
            if (pd.temperatureBar != null) {
                // send to client that the bossbar is removed
                pd.temperatureBar!!.remove()

                // null reference to temperature bar
                pd.temperatureBar = null
            }
        }
    }

    init {
        init()
    }
}