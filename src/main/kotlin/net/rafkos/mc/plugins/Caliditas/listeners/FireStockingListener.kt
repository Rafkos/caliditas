/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas.listeners

import net.rafkos.mc.plugins.Caliditas.Config
import net.rafkos.mc.plugins.Caliditas.listeners.BurningFurnacesListener.Companion.isMaterialFuel
import net.rafkos.mc.plugins.Caliditas.loaders.LoadersManagerHelper.getWorldConfigLoader
import org.bukkit.Bukkit
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.block.data.type.Fire
import org.bukkit.entity.EntityType
import org.bukkit.entity.Item
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.entity.EntityDamageEvent

/**
 * "Fire Stocking" is a feature that makes fire burn longer
 * when sticks or other capable items are thrown into it.
 *
 * @author thebadogre
 */
class FireStockingListener : Listener {
    @EventHandler
    fun onEntityDamage(e: EntityDamageEvent) {
        // check if plugin is enabled or the feature is enabled
        if (!Config.pluginInstance.internalEnabled || !getWorldConfigLoader(e.entity.world).featureFireStockingEnabled) {
            stopAllFireHandlers()
            return
        }

        // check if damage occurred for dropped item
        if (e.entityType == EntityType.DROPPED_ITEM) {
            // check if damager is fire
            val damager = e.entity.location.block
            if (damager.blockData.material == Material.FIRE) {
                val item = e.entity as Item
                val itemMaterial = item.itemStack.type

                // check if dropped item is fuel
                if (isMaterialFuel(itemMaterial)) {
                    assignFireMaintainer(damager.location)
                }
            }
        }
    }

    private fun assignFireMaintainer(location: Location) {
        // only one maintainer per fire
        if (locationToActiveMaintainers.containsKey(location)) {
            locationToActiveMaintainers[location]!!.stop()
        }
        val newMaintainer = FireMaintainer(maintainFireTicks, location)

        // register a new maintainer
        locationToActiveMaintainers[location] = newMaintainer
        Bukkit.getScheduler().runTask(Config.pluginInstance, newMaintainer)
    }

    private inner class FireMaintainer(private var ticksToMaintain: Long, private val fireLocation: Location) : Runnable {
        private var disabled = false
        override fun run() {
            if (disabled) {
                return
            }

            // check if maintainer conditions are met
            if (fireStillExists() && ticksToMaintain > 1) {
                ticksToMaintain--

                // set age of fire
                val fire = fireLocation.block.blockData as Fire
                fire.age = 0
                fireLocation.block.blockData = fire

                // if not last tick then create next maintainer
                if (ticksToMaintain > 0) {
                    scheduleNextRunnable()
                }
            } else {
                unregister()
            }
        }

        private fun unregister() {
            // unregister the old maintainer
            locationToActiveMaintainers.remove(fireLocation)
        }

        private fun scheduleNextRunnable() {
            val nextFireMaintainer = FireMaintainer(ticksToMaintain, fireLocation)

            // register as a new maintainer
            locationToActiveMaintainers[fireLocation] = nextFireMaintainer

            // execute next tick
            Bukkit.getScheduler().runTaskLater(Config.pluginInstance, nextFireMaintainer, 1)
        }

        private fun fireStillExists(): Boolean {
            return fireLocation.block.blockData.material == Material.FIRE
        }

        fun stop() {
            // make sure next tick will disable the maintainer
            disabled = true

            // unregister the maintainer
            unregister()
        }

        init {
            require(ticksToMaintain > 0) { "Ticks to maintain must be > 0" }
        }
    }

    companion object {
        private const val maintainFireTicks: Long = 1200
        private val locationToActiveMaintainers = hashMapOf<Location, FireMaintainer>()

        fun stopAllFireHandlers() {
            for (fm in locationToActiveMaintainers.values) {
                fm.stop()
            }
        }
    }
}