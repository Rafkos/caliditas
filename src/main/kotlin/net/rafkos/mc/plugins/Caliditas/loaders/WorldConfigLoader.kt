/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas.loaders

import com.google.gson.JsonIOException
import com.google.gson.JsonSyntaxException
import net.rafkos.mc.plugins.Caliditas.Config
import net.rafkos.mc.plugins.Caliditas.TemperatureUnit
import java.io.File
import java.io.FileNotFoundException
import java.io.FileReader

class WorldConfigLoader : Loader {
    /**
     * Default temperature unit that will be assigned to new players
     */
    var defaultTemperatureUnit: TemperatureUnit = TemperatureUnit.CELSIUS

    /**
     * This variable is used to slow down temperature increase/decrease per tick.
     * Basically each step is calculated from current temperature of a player and his target
     * temperature (basically step is a delta) then the result is divided by this value.
     *
     * @see net.rafkos.mc.plugins.Caliditas.handlers.FactorsHandler
     */
    var tempStepDivisor = 0.0

    /**
     * This value represents a minimum value of a temperature step
     *
     * @see net.rafkos.mc.plugins.Caliditas.handlers.FactorsHandler
     */
    var minimumTempStep = 0.0

    /**
     * This value represents a maximum value of a temperature step
     *
     * @see net.rafkos.mc.plugins.Caliditas.handlers.FactorsHandler
     */
    var maximumTempStep = 0.0

    var featureBurningFurnacesEnabled = false

    /**
     * This value represents default duration of CONSUMED_* flags when no other value is specified in flags_duration.json.
     */
    var defaultConsumableFlagsDurationTicks = 200

    /**
     * @see net.rafkos.mc.plugins.Caliditas.listeners.FireStockingListener
     */
    var featureFireStockingEnabled = false

    /**
     * Indicates if temperature change messages should not be displayed in chat.
     */
    var disableTemperatureStateChangeChatMessages = false

    override fun load(dataFolder: File): Boolean {
        val miscFile = File(dataFolder, "/misc_config.json")
        try {
            val conf = Config.gson.fromJson(FileReader(miscFile), ReceivedConfig::class.java)
            if (Config.temperatureConverters.containsKey(conf.defaultTempUnit)) {
                defaultTemperatureUnit = conf.defaultTempUnit
            } else {
                return false
            }
            if (conf.tempStepDivisor > 0
                    && conf.maximumTempStep > 0
                    && conf.minimumTempStep > 0
                    && conf.defaultConsumableFlagsDurationSeconds > 0
                    && conf.maximumTempStep > conf.minimumTempStep) {
                tempStepDivisor = conf.tempStepDivisor
                maximumTempStep = conf.maximumTempStep
                minimumTempStep = conf.minimumTempStep
                featureBurningFurnacesEnabled = conf.featureBurningFurnacesEnabled
                featureFireStockingEnabled = conf.featureFireStockingEnabled
                defaultConsumableFlagsDurationTicks = conf.defaultConsumableFlagsDurationSeconds * 20
                disableTemperatureStateChangeChatMessages = conf.disableTemperatureStateChangeChatMessages
            } else {
                return false
            }
        } catch (e: JsonSyntaxException) {
            Config.pluginInstance.logger.severe("Unable to load " + miscFile.path + "" +
                    " file due to incorrect JSON syntax.")
            return false
        } catch (e: JsonIOException) {
            Config.pluginInstance.logger.severe("Unable to load " + miscFile.path + "" +
                    " file due to incorrect JSON syntax.")
            return false
        } catch (e: FileNotFoundException) {
            Config.pluginInstance.logger.info("File " + miscFile.path + "" +
                    " does not exist.")
            return false
        }
        return true
    }

    /**
     * Class used by GSON parser for loading data from Json files.
     *
     * @author thebadogre
     */
    private class ReceivedConfig(
            val defaultTempUnit: TemperatureUnit,
            val tempStepDivisor: Double,
            val minimumTempStep: Double,
            val maximumTempStep: Double,
            val defaultConsumableFlagsDurationSeconds: Int,
            val featureBurningFurnacesEnabled: Boolean,
            val featureFireStockingEnabled: Boolean,
            val disableTemperatureStateChangeChatMessages: Boolean)
}