package net.rafkos.mc.plugins.Caliditas

enum class TemperatureUnit {
    CELSIUS, FAHRENHEIT
}