package net.rafkos.mc.plugins.Caliditas.handlers

import net.rafkos.mc.plugins.Caliditas.listeners.BurningFurnacesListener
import org.bukkit.Material
import org.bukkit.block.Furnace

class BurningFurnacesHandler(val furnacesListener: BurningFurnacesListener): Runnable {
    override fun run() {
        val toRemove = mutableListOf<BurningFurnacesListener.FurnaceCheckSchedule>()
        furnacesListener.furnacesToCheck.forEach { check ->
            when {
                check.ticksTillCheck > 0 -> check.ticksTillCheck -= 1
                else -> toRemove.add(check)
            }
        }

        toRemove.forEach { check ->
            furnacesListener.furnacesToCheck.removeIf { it.location == check.location }
            val block = check.location.block
            if (block.blockData.material == Material.FURNACE) {
                val state = block.state
                if (state is Furnace)
                    furnacesListener.handle(state.inventory)
            }
        }
    }
}