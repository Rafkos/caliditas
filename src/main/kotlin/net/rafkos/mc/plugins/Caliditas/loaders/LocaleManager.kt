package net.rafkos.mc.plugins.Caliditas.loaders

import net.rafkos.mc.plugins.Caliditas.Config
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

/**
 * Purpose of this class is to wrap generic LocaleLoader and world specific LocaleLoaders. When entry does not
 * exist in world specific loader it will try to use generic one instead.
 *
 * @see LocaleLoader
 */
object LocaleManager {
    private var genericLoader: LocaleLoader = LocaleLoader()

    fun loadGenericLocale(): Boolean = genericLoader.load(Config.pluginInstance.dataFolder)

    /**
     * A simple function for obtaining localized strings from string ID,
     * command sender's language and arguments.
     *
     * @param name   - String ID.
     * @param sender - To who the message is addressed.
     * @param args   - Arguments to be inserted into the localized string (if any).
     * @return - Returns localized text (based on sender's language) with arguments inserted.
     */
    fun getLocale(name: String, sender: CommandSender, vararg args: Any?): String {
        var text = name

        // try to obtain world specific locale.
        if (sender is Player) {
            text = LoadersManagerHelper.getLocaleLoader(sender.world).getLocale(name, sender, *args)
        }

        // when no locale found
        if (text == name) {
            text = genericLoader.getLocale(name, sender, *args)
        }
        return text
    }
}