/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas.loaders

import com.google.gson.JsonIOException
import com.google.gson.JsonSyntaxException
import net.rafkos.mc.plugins.Caliditas.Config
import net.rafkos.mc.plugins.Caliditas.HeatSource
import org.bukkit.Material
import java.io.File
import java.io.FileNotFoundException
import java.io.FileReader

class HeatSourceLoader : Loader {
    val sources = hashSetOf<HeatSource>()

    override fun load(dataFolder: File): Boolean {
        sources.clear()
        var success = false
        val heatSourcesFile = File(dataFolder.path, "heat_sources.json")
        try {
            val data = Config.gson.fromJson(FileReader(heatSourcesFile), Array<ReceivedJson>::class.java)
            for (s in data) {
                require(validate(s)) { "Incorrect values in heat_sources.json" }
                sources.add(HeatSource(Material.getMaterial(s.materialName)!!, s.radius))
            }
            success = true
        } catch (e: JsonSyntaxException) {
            Config.pluginInstance.logger.severe("Unable to load " + heatSourcesFile.path + "" +
                    " file due to incorrect JSON syntax.")
        } catch (e: JsonIOException) {
            Config.pluginInstance.logger.severe("Unable to load " + heatSourcesFile.path + "" +
                    " file due to incorrect JSON syntax.")
        } catch (e: FileNotFoundException) {
            Config.pluginInstance.logger.info("File " + heatSourcesFile.path + "" +
                    " does not exist.")
        }
        return success
    }

    private data class ReceivedJson(
            val materialName: String,
            val radius: Int)

    companion object {
        private fun validate(s: ReceivedJson): Boolean {
            return when {
                Material.getMaterial(s.materialName) == null -> {
                    Config.pluginInstance.logger.severe("Heat source material with name \"${s.materialName}\" does not exist.")
                    false
                }
                s.radius < 0 -> {
                    Config.pluginInstance.logger.severe("Heat source radius must be greater than 0 and less or equal ${Config.maxHeatSourceRadius}.")
                    false
                }
                s.radius > Config.maxHeatSourceRadius -> {
                    Config.pluginInstance.logger.severe("Maximum heat source radius exceeded: ${s.radius}. Maximum value is ${Config.maxHeatSourceRadius}.")
                    false
                }
                else -> true
            }
        }
    }
}