/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas.commands

import org.bukkit.command.CommandSender
import org.bukkit.permissions.Permission
import java.util.*

/**
 * This class represents a command.
 *
 * @author thebadogre
 */
abstract class PluginCommand {
    /**
     * Executes the command with given arguments.
     *
     * @param executor - Player or server executing the command
     * @param args     - Arguments
     * @return Returns true if parser correctly.
     */
    abstract fun execute(executor: CommandSender, args: LinkedList<String>): Boolean

    /**
     * @return Returns permissions required by the command to be executed.
     */
    abstract fun getPermissions(): Array<Permission>

    /**
     * @param executor - Player or server.
     * @return Returns true if command sender has permissions to execute this command.
     */
    fun hasPermissions(executor: CommandSender): Boolean {
        var flag = false
        // OP has all permissions
        if (executor.isOp) {
            flag = true
        }
        for (p in getPermissions()) {
            if (executor.hasPermission(p)) {
                flag = true
            }
        }
        return flag
    }

    companion object {
        val userPermission = Permission("caliditas.user")
        val adminPermission = Permission("caliditas.admin")
    }
}