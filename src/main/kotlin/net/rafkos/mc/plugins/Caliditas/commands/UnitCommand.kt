/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas.commands

import net.rafkos.mc.plugins.Caliditas.Config
import net.rafkos.mc.plugins.Caliditas.TemperatureUnit
import net.rafkos.mc.plugins.Caliditas.loaders.LoadersManagerHelper
import net.rafkos.mc.plugins.Caliditas.loaders.LocaleManager
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import org.bukkit.permissions.Permission
import java.util.*

class UnitCommand : PluginCommand() {
    override fun execute(executor: CommandSender, args: LinkedList<String>): Boolean {
        if (executor is Player && args.size == 1) {
            val unit: TemperatureUnit? = TemperatureUnit.valueOf(args.poll().toUpperCase())
            return if (unit != null && Config.temperatureConverters.containsKey(unit)) {
                val pd = LoadersManagerHelper.playerDataLoader.getPlayerData(executor)
                pd.temperatureUnit = unit
                executor.sendMessage(LocaleManager.getLocale("CMD_CALI_UNIT_SUCCESS", executor, unit.toString().toLowerCase()))
                true
            } else {
                executor.sendMessage(LocaleManager.getLocale("CMD_CALI_UNIT_FAIL", executor, unit.toString().toLowerCase()))
                true
            }
        }
        return false
    }

    override fun getPermissions(): Array<Permission> {
        return arrayOf(permission, userPermission, adminPermission)
    }

    companion object {
        val permission = Permission("caliditas.unit")
    }
}