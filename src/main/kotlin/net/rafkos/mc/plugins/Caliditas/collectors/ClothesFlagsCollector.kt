/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas.collectors

import net.rafkos.mc.plugins.Caliditas.Flag
import net.rafkos.mc.plugins.Caliditas.loaders.LoadersManagerHelper
import org.bukkit.entity.Player
import org.bukkit.inventory.ItemStack

/**
 * Collects flags from clothes of a player.
 *
 * @author thebadogre
 */
class ClothesFlagsCollector : FlagsCollector {
    override fun collectFlags(player: Player) {
        val pd = LoadersManagerHelper.playerDataLoader.getPlayerData(player)
        val helmet: ItemStack? = player.inventory.helmet
        val boots: ItemStack? = player.inventory.boots
        val leggings: ItemStack? = player.inventory.leggings
        val chestplate: ItemStack? = player.inventory.chestplate
        pd.addFlag(if (helmet != null) Flag("WEARING_" + helmet.type.toString().toUpperCase(), player.world) else Flag("WEARING_NO_HELMET", player.world))
        pd.addFlag(if (boots != null) Flag("WEARING_" + boots.type.toString().toUpperCase(), player.world) else Flag("WEARING_NO_BOOTS", player.world))
        pd.addFlag(if (leggings != null) Flag("WEARING_" + leggings.type.toString().toUpperCase(), player.world) else Flag("WEARING_NO_LEGGINGS", player.world))
        pd.addFlag(if (chestplate != null) Flag("WEARING_" + chestplate.type.toString().toUpperCase(), player.world) else Flag("WEARING_NO_CHESTPLATE", player.world))
    }
}