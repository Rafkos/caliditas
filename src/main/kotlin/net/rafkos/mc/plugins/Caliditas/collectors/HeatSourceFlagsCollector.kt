/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas.collectors

import net.rafkos.mc.plugins.Caliditas.Config
import net.rafkos.mc.plugins.Caliditas.Flag
import net.rafkos.mc.plugins.Caliditas.HeatSource
import net.rafkos.mc.plugins.Caliditas.loaders.LoadersManagerHelper
import org.bukkit.Material
import org.bukkit.entity.Player

/**
 * Collects flags from heat sources.
 *
 * @author thebadogre
 */
class HeatSourceFlagsCollector : FlagsCollector {
    /**
     * The lower the index is the more important the flag is.
     *
     * @see HeatSourceFlagsCollector.getMostImportantFlag
     */
    private val importanceList = arrayOf(
            Flag("STANDING_IN_LAVA", 1),
            Flag("STANDING_NEAR_LAVA", 1),
            Flag("STANDING_ON_HEAT_SOURCE", 1),
            Flag("STANDING_NEAR_HEAT_SOURCE", 1),
            Flag("STANDING_AROUND_HEAT_SOURCE", 1),
            Flag("HEAT_FROM_TORCH", 1))

    override fun collectFlags(player: Player) {
        val heatSources = LoadersManagerHelper.getHeatSourceLoader(player.world).sources

        // no heat sources
        if (heatSources.isEmpty())
            return

        val pd = LoadersManagerHelper.playerDataLoader.getPlayerData(player)

        // list of all collected flags
        val flags = hashSetOf<Flag>()

        // calculte distances to heat sources
        val heatSourceDistances = distanceFromHeatSourcesInRadius(player, heatSources)
        val heatSourceRadiusMap = hashMapOf<Material, Int>()
                .also { map -> heatSources.forEach { map[it.material] = it.radius } }

        // check every possible heat source
        for (s in heatSourceDistances) {
            val material = s.key
            val distanceFromHeatSource = s.value

            // distance -1.0 indicates that the heat source wasn't found
            if (distanceFromHeatSource != -1.0) {
                pd.addFlag(Flag("INFLUENCED_BY_ANY_HEAT_SOURCE", player.world))

                // distance does not matter for heat from a torch
                if (material == Material.TORCH || material == Material.WALL_TORCH) {
                    flags.add(Flag("HEAT_FROM_TORCH", player.world))
                } else {
                    // if the player is standing on the block that generates heat
                    if (distanceFromHeatSource < 1) {
                        if (material == Material.LAVA) {
                            flags.add(Flag("STANDING_IN_LAVA", player.world))
                        } else {
                            flags.add(Flag("STANDING_ON_HEAT_SOURCE", player.world))
                        }
                    } else if (distanceFromHeatSource < heatSourceRadiusMap[material]!! * 0.65) {
                        if (material == Material.LAVA) {
                            flags.add(Flag("STANDING_NEAR_LAVA", player.world))
                        } else {
                            flags.add(Flag("STANDING_NEAR_HEAT_SOURCE", player.world))
                        }
                    } else {
                        flags.add(Flag("STANDING_AROUND_HEAT_SOURCE", player.world))
                    }
                }
            }
        }

        // from all the collected flags get one that is most important
        // (in other words the one that has strongest impact on the player)
        val flag = getMostImportantFlag(flags)
        if (flag != null) {
            pd.addFlag(flag)
        }
    }

    private fun getMostImportantFlag(flags: MutableSet<Flag>): Flag? {
        var result: Flag? = null
        for (f in flags) {
            if (result == null) {
                result = f
            } else if (isMoreImportant(f, result)) {
                result = f
            }
        }
        return result
    }

    /**
     * Determines the most important flag based on the importanceList
     *
     * @param newf - New flag
     * @param oldf - Old flag
     * @return - Returns true if the new flag is more important or equal.
     * @see HeatSourceFlagsCollector.importanceList
     */
    private fun isMoreImportant(newf: Flag, oldf: Flag): Boolean {
        var indexNew = 0
        var indexOld = 0
        for ((counter, f) in importanceList.withIndex()) {
            if (f == newf) {
                indexNew = counter
            }
            if (f == oldf) {
                indexOld = counter
            }
        }
        return indexNew <= indexOld
    }

    /**
     * Returns a map of smallest distances to provided heat materials from player's location. Distance = -1.0 when
     * no block of given material found.
     */
    private fun distanceFromHeatSourcesInRadius(player: Player, heatSources: Set<HeatSource>): Map<Material, Double> {
        val playerLocation = player.location
        // map of smallest distances
        val distances = hashMapOf<Material, Double>()
        val materials = hashSetOf<Material>()
        heatSources.forEach { s ->
                distances[s.material] = -(1.0)
                materials.add(s.material)
        }

        // maximum radius value of heat sources
        val radius = mutableListOf<Int>().also { list -> heatSources.forEach { list.add(it.radius) } }.max()!!

        // check all blocks in radius
        for (x in -radius..radius) {
            for (y in -radius..radius) {
                for (z in -radius..radius) {
                    val b = playerLocation.block.getRelative(x, y, z)
                    // if block is a heat source
                    if (materials.contains(b.blockData.material)) {
                        // calculate distance
                        val distance = playerLocation.distance(b.location)
                        val currentDistance = distances[b.blockData.material]!!

                        // if distance is smaller that previous one then assign
                        if (distance < currentDistance || currentDistance == -1.0) {
                            // an exception where block is a furnace
                            if (b.blockData.material == Material.FURNACE) {
                                if (!Config.pluginInstance.burningFurnacesListener!!.isFurnaceBurning(b.location))
                                    continue
                            }
                            distances[b.blockData.material] = distance
                        }
                    }
                }
            }
        }
        return distances.toMap()
    }
}