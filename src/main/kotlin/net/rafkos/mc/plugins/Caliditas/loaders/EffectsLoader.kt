/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas.loaders

import com.google.gson.JsonIOException
import com.google.gson.JsonSyntaxException
import net.rafkos.mc.plugins.Caliditas.Config
import net.rafkos.mc.plugins.Caliditas.TemperatureState
import net.rafkos.mc.plugins.Caliditas.effects.Effect
import org.bukkit.potion.PotionEffectType
import java.io.File
import java.io.FileNotFoundException
import java.io.FileReader

class EffectsLoader : Loader {
    val effects = hashMapOf<TemperatureState, MutableList<Effect>>()

    override fun load(dataFolder: File): Boolean {
        effects.clear()
        var success = false
        val effectsFile = File(dataFolder, "temp_effects.json")
        try {
            val effects = Config.gson.fromJson(FileReader(effectsFile), Array<Effect>::class.java)
            for (e in effects) {
                require(valid(e)) { "Incorrect values in temp_factors.json" }
                if (!this.effects.containsKey(e.tempState)) {
                    this.effects[e.tempState] = mutableListOf()
                }
                this.effects[e.tempState]!!.add(e)
            }
            success = true
        } catch (e: JsonSyntaxException) {
            Config.pluginInstance.logger.severe("Unable to load " + effectsFile.path + "" +
                    " file due to incorrect JSON syntax.")
        } catch (e: JsonIOException) {
            Config.pluginInstance.logger.severe("Unable to load " + effectsFile.path + "" +
                    " file due to incorrect JSON syntax.")
        } catch (e: FileNotFoundException) {
            Config.pluginInstance.logger.info("File " + effectsFile.path + "" +
                    " does not exist.")
        }
        return success
    }

    private fun valid(e: Effect): Boolean {
        if (e.durationSeconds <= 0) {
            Config.pluginInstance.logger.severe("Incorrect value of effect duration. Value should be > 0.")
            return false
        }
        if (e.effectAmplifier <= 0) {
            Config.pluginInstance.logger.severe("Incorrect value of effect amplifier. Value should be > 0.")
            return false
        }
        if (PotionEffectType.getByName(e.effectType) == null) {
            Config.pluginInstance.logger.severe("Incorrect value of effect type " + e.effectType
                    + ". Potion effect of this type does not exist..")
            return false
        }
        return true
    }
}