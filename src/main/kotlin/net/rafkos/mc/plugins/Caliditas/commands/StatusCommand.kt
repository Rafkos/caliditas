/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas.commands

import net.rafkos.mc.plugins.Caliditas.MessagesGenerator
import net.rafkos.mc.plugins.Caliditas.loaders.LoadersManagerHelper
import net.rafkos.mc.plugins.Caliditas.loaders.LocaleManager
import org.bukkit.Bukkit
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import org.bukkit.permissions.Permission
import java.util.*

class StatusCommand : PluginCommand() {
    override fun execute(executor: CommandSender, args: LinkedList<String>): Boolean {
        if (args.isEmpty() && executor is Player) {
            showTemperatureStatus(executor, executor)
            return true
        }
        if (!args.isEmpty() && (executor.hasPermission(permissionCheckOther) || executor.hasPermission(adminPermission))) {
            val player = Bukkit.getServer().getPlayer(args.first)
            if (player != null) {
                showTemperatureStatus(player, executor)
                return true
            } else {
                executor.sendMessage(LocaleManager.getLocale("CMD_PLAYER_NOT_FOUND", executor))
            }
        }
        return false
    }

    private fun showTemperatureStatus(player: Player, executor: CommandSender) {
        val pd = LoadersManagerHelper.playerDataLoader.getPlayerData(player)
        if (pd.isTemperatureImmune) {
            MessagesGenerator.immuneMessage(player, executor)
        } else {
            executor.sendMessage(LocaleManager.getLocale("CMD_STATUS_OF", executor, player.name))
            MessagesGenerator.factorsTable(player, executor)
            MessagesGenerator.currentTemperature(player, executor)
            MessagesGenerator.targetTemperature(player, executor)
            MessagesGenerator.temperatureChange(player, executor)
            MessagesGenerator.temperatureState(player, executor)
        }
    }

    override fun getPermissions(): Array<Permission> {
        return arrayOf(permission, userPermission, adminPermission)
    }

    companion object {
        val permission = Permission("caliditas.status")
        val permissionCheckOther = Permission("caliditas.status.other")
    }
}