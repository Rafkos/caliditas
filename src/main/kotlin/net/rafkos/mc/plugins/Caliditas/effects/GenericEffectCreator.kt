/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas.effects

import net.rafkos.mc.plugins.Caliditas.TemperatureState
import net.rafkos.mc.plugins.Caliditas.events.PlayerTemperatureEffectAdded
import net.rafkos.mc.plugins.Caliditas.loaders.LoadersManagerHelper
import org.bukkit.Bukkit
import org.bukkit.entity.Player
import org.bukkit.potion.PotionEffect

class GenericEffectCreator : EffectCreator {
    /**
     *  Adds temperature effects of specific temoperature state to given player.
     */
    override fun setTemperatureEffect(p: Player, s: TemperatureState) {
        val loader = LoadersManagerHelper.getEffectsLoader(p.world)
        if (loader.effects.containsKey(s)) {
            for (e in loader.effects[s]!!) {
                // add the effect only if player does not have it
                // or add it if the effect the player has is the same but has lower strength
                if (!p.hasPotionEffect(e.potionEffectType())
                        || (p.hasPotionEffect(e.potionEffectType())
                                && e.effectAmplifier > p.getPotionEffect(e.potionEffectType())!!.amplifier)) {
                    val pe = PotionEffect(e.potionEffectType(),
                            e.durationSeconds * 20, e.effectAmplifier)

                    // register event
                    val event = PlayerTemperatureEffectAdded(p, pe)
                    Bukkit.getPluginManager().callEvent(event)
                    if (!event.cancelled) {
                        p.addPotionEffect(pe)
                    }
                }
            }
        }
    }
}