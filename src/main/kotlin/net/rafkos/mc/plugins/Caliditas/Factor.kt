/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas

import net.rafkos.utils.booltree.Condition

/**
 * This class represents a temperature factor. Factors are used by FactorsHandler
 * to determine player's target temperature.
 *
 * @author thebadogre
 */
class Factor(
        /**
         * Name of a factor (localisation string ID). Used by localizations.
         *
         * @see net.rafkos.mc.plugins.Caliditas.loaders.LocaleLoader
         */
        val factorName: String,

        /**
         * Basically how much does the factor change player's internal temperature.
         * It is used to calculate player's target temperature
         *
         * @see net.rafkos.mc.plugins.Caliditas.handlers.FactorsHandler
         */
        val tempChange: Double,

        /**
         * This condition determines rules for the factor to occur.
         *
         * @see net.rafkos.mc.plugins.Caliditas.handlers.FactorsHandler
         */
        val condition: Condition

) {
    override fun toString(): String = factorName

    override fun equals(other: Any?): Boolean {
        if (other !is Factor) return false
        return factorName == other.factorName && tempChange == other.tempChange && condition == other.condition
    }

    override fun hashCode(): Int {
        var result = factorName.hashCode()
        result = 31 * result + tempChange.hashCode()
        result = 31 * result + condition.hashCode()
        return result
    }
}