/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas.collectors

import net.rafkos.mc.plugins.Caliditas.Flag
import net.rafkos.mc.plugins.Caliditas.PlayerData
import net.rafkos.mc.plugins.Caliditas.loaders.LoadersManagerHelper
import org.bukkit.World
import org.bukkit.entity.Player

/**
 * Calculates flags from weather for a player.
 *
 * @author thebadogre
 */
class WeatherFlagsCollector : FlagsCollector {
    override fun collectFlags(player: Player) {
        val pd = LoadersManagerHelper.playerDataLoader.getPlayerData(player)

        // collect flags from rain/snow
        collectFlagsFromRainOrSnow(player, pd)
        if (player.world.hasStorm()) {
            pd.addFlag(Flag("STORM", player.world))
        }
        if (player.world.isThundering) {
            pd.addFlag(Flag("THUNDERING", player.world))
        }
    }

    private fun collectFlagsFromRainOrSnow(player: Player, pd: PlayerData) {
        // it can only rain/snow in normal environment
        if (player.world.environment == World.Environment.NORMAL) {

            // check if it's possible for the rain/snow to fall in given biome temperature (vanilla minecraft temperature)
//            if (player.world.getTemperature(
//                            player.location.blockX,
//                            player.location.blockY,
//                            player.location.blockZ) < 1.0) {
            @Suppress("DEPRECATION")
            if (player.world.getTemperature(      // Deprecated method used to maintain backwards compatibility
                            player.location.blockX,
                            player.location.blockZ) < 1.0) {
                // it can only rain/snow when storm
                if (player.world.hasStorm()) {
                    // check if player has no blocks above
                    if (!PlayerSurroundingsFlagsCollector.playerUnderRoof(player)) {
                        pd.addFlag(Flag("RAINING_OR_SNOWING_ON_PLAYER", player.world))
                    }
                }
            }
        }
    }
}