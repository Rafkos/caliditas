/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas.commands

import net.rafkos.mc.plugins.Caliditas.loaders.LoadersManagerHelper
import net.rafkos.mc.plugins.Caliditas.loaders.LocaleManager
import org.bukkit.Bukkit
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import org.bukkit.permissions.Permission
import java.util.*

class BarCommand : PluginCommand() {
    override fun execute(executor: CommandSender, args: LinkedList<String>): Boolean {
        if (args.size == 1 && executor is Player) {
            return changeBarStatus(executor, args.poll())
        } else if (args.size == 2 && (executor.hasPermission(permissionOtherBar) || executor.hasPermission(adminPermission))) {
            val player = Bukkit.getServer().getPlayer(Objects.requireNonNull(args.poll()))
            if (player != null) {
                return changeBarStatus(player, args.poll())
            } else {
                executor.sendMessage(LocaleManager.getLocale("CMD_PLAYER_NOT_FOUND", executor))
            }
        }
        return false
    }

    private fun changeBarStatus(player: Player, arg: String): Boolean {
        val pd = LoadersManagerHelper.playerDataLoader.getPlayerData(player)
        val flag: Boolean = try {
            java.lang.Boolean.parseBoolean(arg)
        } catch (e: Exception) {
            player.sendMessage(LocaleManager.getLocale("CMD_SYNTAX_ERROR", player))
            return false
        }
        player.sendMessage(LocaleManager.getLocale("CMD_BAR_STATUS_CHANGED", player, player.name, flag))
        pd.temperatureBarEnabled = flag
        return true
    }

    override fun getPermissions(): Array<Permission> {
        return arrayOf(permission, userPermission, adminPermission)
    }

    companion object {
        val permission = Permission("caliditas.bar")
        val permissionOtherBar = Permission("caliditas.bar.other")
    }
}