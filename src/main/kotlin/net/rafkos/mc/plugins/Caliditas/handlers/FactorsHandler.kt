/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas.handlers

import net.rafkos.mc.plugins.Caliditas.*
import net.rafkos.mc.plugins.Caliditas.events.PlayerTemperatureChanged
import net.rafkos.mc.plugins.Caliditas.loaders.LoadersManagerHelper
import org.bukkit.Bukkit
import org.bukkit.World
import org.bukkit.entity.Player
import kotlin.math.abs

/**
 * FactorsHandler handles temperature calculations for all online players.
 * It calculates temperature step and target temperature based on flags and factors.
 *
 * @author thebadogre
 * @see net.rafkos.mc.plugins.Caliditas.loaders.FactorsLoader
 */
class FactorsHandler : Runnable {
    override fun run() {
        if (Config.stressTest) {
            for (i in 0..Config.stressTestPasses)
                execute()
        } else {
            execute()
        }
    }

    private fun execute() {
        // check if plugin is enabled
        if (!Config.pluginInstance.internalEnabled) {
            return
        }

        // handle online players
        for (player in Bukkit.getServer().onlinePlayers) {
            handlePlayer(player)
        }
    }

    /**
     * This function handles player's temperature. It stores player's
     * data such as temperature, state, step etc. from previous tick and assigns new temperature.
     *
     * @param player - Player to handle.
     */
    private fun handlePlayer(player: Player) {

        // gets player data
        val pd: PlayerData = LoadersManagerHelper.playerDataLoader.getPlayerData(player)

        // if player is immune to temperature then reset his temperature data
        if (pd.isTemperatureImmune) {
            pd.temperature = LoadersManagerHelper.getTemperatureStateLoader(Bukkit.getPlayer(pd.uniqueId)!!.world).states[TemperatureState.NORMAL]!!
            pd.flags.clear()
            pd.state = TemperatureState.NORMAL
            pd.lastState = TemperatureState.NORMAL
            pd.lastStep = 0.0

            // stop handling this player
            return
        }

        // assign flags to player
        for (collector in Config.flagsCollectors) {
            collector.collectFlags(player)
        }

        // gets current biome of player
        val playerBiome = player.location.block.biome
        val bd: BiomeData?
        bd = if (LoadersManagerHelper.getBiomesDataLoader(player.world).biomeData.containsKey(playerBiome.toString())) {
            LoadersManagerHelper.getBiomesDataLoader(player.world).biomeData[playerBiome.toString()]
        } else {
            // if for some reason biome data is not present in json config files it will
            // use generic one instead
            LoadersManagerHelper.getBiomesDataLoader(player.world).biomeData["GENERIC"]
        }

        // assign flags from biome to player
        for (biomeFlag in bd!!.flags) {
            pd.addFlag(biomeFlag)
        }

        // alters flag durations if world has changed
        pd.flags.forEach { alterFlagDuration(it, player.world) }

        val wc = LoadersManagerHelper.getWorldConfigLoader(player.world)
        val temperatureChange = calculateTemperatureChange(pd)
        val biomeTemperature = bd.targetBodyTemp
        val targetTemperature = calculateTargetTemperature(biomeTemperature, temperatureChange)
        val step = calculateStep(targetTemperature - pd.temperature, wc.tempStepDivisor, wc.maximumTempStep, wc.minimumTempStep)
        pd.lastTemperatureChange = temperatureChange
        pd.lastStep = step
        pd.lastTargetTemperature = targetTemperature
        pd.lastTemperature = pd.temperature
        val lastFlags = hashSetOf<Flag>()
        pd.flags.forEach { lastFlags.add(it.clone()) }
        pd.lastFlags = lastFlags
        pd.flags.forEach { it.durationTicks-- }
        val remainingFlags = pd.flags.filter { it.durationTicks > 0 }
        pd.flags.clear()
        pd.flags.addAll(remainingFlags)

        // register event
        val event = PlayerTemperatureChanged(player, pd.temperature + step)
        Bukkit.getPluginManager().callEvent(event)
        if (!event.cancelled) {
            // assign new temperature to player
            pd.temperature = event.temperature
        }
    }

    /**
     * This function will reduce flag's duration to world's maximum level.
     */
    private fun alterFlagDuration(f: Flag, w: World) {
        val maxFlagDuration = LoadersManagerHelper.getFlagsDurationLoader(w).durationMap[f.flagName]
        val defaultDuration = LoadersManagerHelper.getWorldConfigLoader(w).defaultConsumableFlagsDurationTicks
        val maxDuration = maxFlagDuration ?: defaultDuration
        if (f.durationTicks > maxDuration) {
            f.durationTicks = maxDuration
        }
    }

    /**
     * This function calculates temperature change of biome temperature based on
     * player factors collected from flags.
     *
     * @param pd - Player's data.
     * @return returns calculated temperature change.
     */
    private fun calculateTemperatureChange(pd: PlayerData): Double {
        val factors = collectFactors(pd)
        var temperatureChange = 0.0
        for (f in factors) {
            temperatureChange += f.tempChange
        }
        pd.lastFactors = mutableListOf<Factor>().also { it.addAll(factors) }
        return temperatureChange
    }

    /**
     * This function collects factors from flags provided in PlayerData and merges them with active factors.
     * This function also updates active factors with newly created ones.
     *
     * @param pd - Player's data
     * @return returns a set of compatible factors.
     */
    private fun collectFactors(pd: PlayerData): MutableList<Factor> {
        val factors = mutableListOf<Factor>()
        for (f in LoadersManagerHelper.getFactorsLoader(Bukkit.getPlayer(pd.uniqueId)!!.world).factors) {
            // check if factor is compatible with player's flags
            if (isFactorCompatibleWithFlags(f, pd.flags)) {
                factors.add(f)
            }
        }
        return factors
    }

    /**
     * This function checks if given factor is compatible with given list of flags.
     *
     * @param factor      - Factor to be checked for flags compatibility.
     * @param playerFlags - Player flags.
     * @return returns true if compatible.
     */
    private fun isFactorCompatibleWithFlags(factor: Factor, playerFlags: MutableSet<Flag>): Boolean {
        val variables = hashMapOf<String, Boolean>()
        // build variable map
        factor.condition.variables.forEach { variables[it] = playerFlags.contains(Flag(it, 1)) }
        return factor.condition.evaluate(variables)
    }

    /**
     * Calculates target temperature based on biome temperature and temperature change.
     * Additionally it checks if the temperature is in correct range (defined in config)
     *
     * @param biomeTemperature  - temperature of a biome.
     * @param temperatureChange - temperature change (calculated from factors).
     * @return - Returns calculated target temperature.
     * @see net.rafkos.mc.plugins.Caliditas.Config
     */
    private fun calculateTargetTemperature(biomeTemperature: Double, temperatureChange: Double): Double {
        // calculate target temperature
        var targetTemperature = biomeTemperature + temperatureChange

        // ensure the temperature is in correct range
        if (targetTemperature > Config.maxTemperature) {
            targetTemperature = Config.maxTemperature
        } else if (targetTemperature < Config.minTemperature) {
            targetTemperature = Config.minTemperature
        }
        return targetTemperature
    }

    /**
     * This function calculates step of a temperature. Step is how much does
     * the temperature change by one tick. It is calculated from temperature delta
     * (target temperature - player's last temperature). The function also
     * ensures that the step is in correct range defined in config.
     *
     * @param temperatureDelta - temperature delta.
     * @param stepDivisor      - Divides temperature delta to make it smaller so the step takes longer in server ticks.
     * @param maximumStep      - Upper threshold.
     * @param minimumStep      - Lower threshold.
     * @return returns calculated step.
     * @see net.rafkos.mc.plugins.Caliditas.Config
     */
    private fun calculateStep(temperatureDelta: Double, stepDivisor: Double, maximumStep: Double, minimumStep: Double): Double {
        var step = temperatureDelta / stepDivisor
        val sign: Double = if (step > 0) 1.toDouble() else (-1).toDouble()
        if (abs(step) > maximumStep) {
            step = sign * maximumStep
        } else if (abs(step) < minimumStep) {
            step = if (abs(temperatureDelta) < minimumStep) {
                temperatureDelta
            } else {
                sign * minimumStep
            }
        }
        return step
    }
}