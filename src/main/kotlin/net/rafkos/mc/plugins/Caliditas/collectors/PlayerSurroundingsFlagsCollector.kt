/**
 * Copyright (C) 2018 Rafał Kosyl, admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.mc.plugins.Caliditas.collectors

import net.rafkos.mc.plugins.Caliditas.Flag
import net.rafkos.mc.plugins.Caliditas.PlayerData
import net.rafkos.mc.plugins.Caliditas.loaders.LoadersManagerHelper.playerDataLoader
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.entity.Player
import org.bukkit.util.BlockIterator
import org.bukkit.util.Vector
import java.util.*

class PlayerSurroundingsFlagsCollector : FlagsCollector {
    override fun collectFlags(player: Player) {
        val pd = playerDataLoader.getPlayerData(player)
        if (playerUnderRoof(player)) {
            pd.addFlag(Flag("UNDER_ROOF", player.world))
        }
        if (playerInOpenSpace(player)) {
            pd.addFlag(Flag("IN_OPEN_SPACE", player.world))
        } else {
            pd.addFlag(Flag("IN_CLOSED_SPACE", player.world))
        }
        collectFlagsFromAltitude(player, pd)
    }

    private fun collectFlagsFromAltitude(player: Player, pd: PlayerData) {
        val altitude = player.location.blockY
        if (altitude >= 100) {
            pd.addFlag(Flag("HIGH_ALTITUDE", player.world))
        } else if (altitude <= 45) {
            pd.addFlag(Flag("LOW_ALTITUDE", player.world))
        }
    }

    private fun playerInOpenSpace(player: Player): Boolean {
        val vectors = calculateVectors()


        // create block iterators based on rays casted by player (vectors)
        val blockIterators = calculateBlockIterators(player.eyeLocation, vectors)

        // calculate blocks of air that are visible from player's perspective
        val visibleBlocks = sumLengthsOfTransparentBlocks(blockIterators)

        // if more than n% of blocks are air blocks then consider it as open space
        return openSpaceThresholdPercentage < visibleBlocks.toDouble() / (openSpaceRadius * blockIterators.size).toDouble()
    }

    private fun sumLengthsOfTransparentBlocks(blockIterators: List<BlockIterator>): Int {
        var visibleBlocks = 0
        for (bi in blockIterators) {
            visibleBlocks += calculateLengthOfTransparentBlocksUntilSolidBlock(bi)
        }
        return visibleBlocks
    }

    private fun calculateLengthOfTransparentBlocksUntilSolidBlock(bi: BlockIterator): Int {
        var visibleBlocks = 0
        while (bi.hasNext()) {
            val b = bi.next()
            if (airMaterials.contains(b.blockData.material)) {
                visibleBlocks++
            } else {
                break
            }
        }
        return visibleBlocks
    }

    private fun calculateBlockIterators(location: Location, vectors: MutableList<Vector>): List<BlockIterator> {
        val blockIterators = mutableListOf<BlockIterator>()
        val startVector = Vector(location.blockX, location.blockY, location.blockZ)
        for (v in vectors) {
            val bi = BlockIterator(Objects.requireNonNull(location.world)!!, startVector, v, 0.0, openSpaceRadius)
            blockIterators.add(bi)
        }
        return blockIterators
    }

    private fun calculateVectors(): MutableList<Vector> {
        val vectors = mutableListOf<Vector>()
        var x = -1.0
        while (x <= 1.0) {
            x += 0.2
            val x2 = x * x
            var y = -1.0
            while (y <= 1.0) {
                y += 0.2
                val y2 = y * y
                var z = -1.0
                while (z <= 1.0) {
                    z += 0.2
                    val c = x2 + y2 + (z * z)
                    if (c in 0.9..1.0) {
                        vectors.add(Vector(x, y, z))
                    }
                }
            }
        }

        return vectors
    }

    companion object {
        /**
         * Number of blocks starting from player in direction of given vector.
         * Used for calculating open space flag.
         */
        private const val openSpaceRadius = 12

        /**
         * Minimum percentage of air blocks in player's radius to consider area an open space.
         */
        private const val openSpaceThresholdPercentage = 0.38

        /**
         * Air materials.
         */
        private val airMaterials = hashSetOf(Material.AIR, Material.CAVE_AIR)

        fun playerUnderRoof(player: Player): Boolean {
            var block = player.eyeLocation.block
            while (block.blockData.material != Material.VOID_AIR) {
                if (!airMaterials.contains(block.blockData.material)) {
                    return true
                }
                block = block.getRelative(0, 1, 0)
            }
            return false
        }
    }
}