/**
 * Copyright (C) 2018 Rafał Kosyl admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.utils.booltree

class ValueLeaf(
        private val variable: String,
        override var negated: Boolean = false) : Leaf {

    override fun evaluate(negated: Boolean, variables: Map<String, Boolean>): Boolean {
        val result = (variables[variable] ?: throw IllegalArgumentException("Variable \"${variable}\" does not exist."))
        return if (negated) result.not() else result
    }

    override fun toString(): String = "${if (negated) "!" else ""}$variable"

    override fun equals(other: Any?): Boolean {
        if (other !is ValueLeaf) return false
        return variable == other.variable && negated == other.negated
    }

    override fun hashCode(): Int {
        var result = variable.hashCode()
        result = 31 * result + negated.hashCode()
        return result
    }
}
