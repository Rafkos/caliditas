/**
 * Copyright (C) 2018 Rafał Kosyl admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.utils.booltree

class AndLeaf(left: Leaf, right: Leaf, negated: Boolean = false)
    : ExpressionLeaf(left, right, negated) {

    override fun evaluate(negated: Boolean, variables: Map<String, Boolean>): Boolean =
            if (negated) left.evaluate(variables).and(right.evaluate(variables)).not()
            else left.evaluate(variables).and(right.evaluate(variables))

    override fun toString(): String = "${if (negated) "!" else ""}(${left} & ${right})"

    override fun equals(other: Any?): Boolean {
        if (other !is AndLeaf) return false
        return left == other.left && right == other.right && negated == other.negated
    }

    override fun hashCode(): Int {
        return javaClass.hashCode()
    }
}