/**
 * Copyright (C) 2018 Rafał Kosyl admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.utils.booltree

class BooleanExpressionSyntaxError(index: Int, expected: String, found: String, expression: String)
    : Exception("\nBoolean expression syntax error at index \"${index}\".\n" +
        "Expected \"$expected\", got \"$found\" in expression:\n\"$expression\".\n" +
        "${drawSpaces(index)}^") {
    companion object {
        private fun drawSpaces(n: Int): String {
            val builder = StringBuilder()
            for (i in 0 until n)
                builder.append(' ')
            return builder.toString()
        }
    }
}

