/**
 * Copyright (C) 2018 Rafał Kosyl admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.utils.booltree

import java.util.concurrent.atomic.AtomicInteger

object Parser {
    private const val NEGATE_SYMBOL = '!'
    private const val AND_SYMBOL = '&'
    private const val OR_SYMBOL = '|'

    fun parseTree(expression: String): Condition {
        val cleaned = expression.replace("[\\n\\s]".toRegex(), "")
        val variables = hashSetOf<String>()
        val leaf = if (cleaned.isEmpty())
            null
        else parseExpression(
                variables,
                AtomicInteger(0),
                cleaned,
                false)
        return Condition(leaf, variables)
    }

    private fun parseExpression(
            variables: MutableSet<String>,
            index: AtomicInteger,
            expression: String,
            negate: Boolean): Leaf? {
        val first = parseLeaf(variables, index, expression)
                ?: throw BooleanExpressionSyntaxError(index.get(), "expression", "null", expression)
        val second: Leaf?
        if (index.get() > expression.length - 1) return first
        return when (val operator = parseOperator(index, expression)) {
            null -> first.also { it.negated = negate }
            else -> {
                index.incrementAndGet()
                second = parseLeaf(variables, index, expression)
                        ?: throw BooleanExpressionSyntaxError(
                                index.get(), "expression", "null", expression)
                var combined = when (operator) {
                    Operator.AND -> AndLeaf(first, second, false)
                    Operator.OR -> OrLeaf(first, second, false)
                    null -> throw BooleanExpressionSyntaxError(
                            index.get() - 1,
                            "operator",
                            "${expression[index.get() - 1]}",
                            expression)
                }

                var secondOperator: Operator?
                do {
                    if (index.get() > expression.length - 1)
                        return combined.also { it.negated = negate }
                    secondOperator = parseOperator(index, expression)
                    index.incrementAndGet()
                    when (secondOperator) {
                        Operator.OR -> {
                            val triple = OrLeaf(combined, parseLeaf(variables, index, expression)
                                    ?: throw BooleanExpressionSyntaxError(
                                            index.get(), "expression", "null", expression))
                            combined = triple
                        }
                        Operator.AND -> {
                            val triple = AndLeaf(combined, parseLeaf(variables, index, expression)
                                    ?: throw BooleanExpressionSyntaxError(
                                            index.get(), "expression", "null", expression))
                            combined = triple
                        }
                        null -> {
                            index.decrementAndGet()
                        }
                    }
                } while (secondOperator != null)
                return combined.also { it.negated = negate }
            }
        }
    }

    private fun parseLeaf(
            variables: MutableSet<String>,
            index: AtomicInteger,
            expression: String): Leaf? {
        val negate = parseNegation(index, expression)

        if (index.get() > expression.length - 1)
            throw BooleanExpressionSyntaxError(
                    index.get() + 1,
                    ")",
                    if (index.get() > expression.length - 1) "end of expression" else "${expression[index.get()]}",
                    expression)
        return when (expression[index.get()]) {
            '(' -> {
                index.incrementAndGet()
                val leaf = parseExpression(variables, index, expression, negate)
                if (index.get() > expression.length - 1 || expression[index.get()] != ')')
                    throw BooleanExpressionSyntaxError(
                            index.get() + 1,
                            ")",
                            if (index.get() > expression.length - 1) "end of expression" else "${expression[index.get()]}",
                            expression)
                index.incrementAndGet()
                leaf
            }
            AND_SYMBOL, OR_SYMBOL, NEGATE_SYMBOL, ')' ->
                throw BooleanExpressionSyntaxError(
                        index.get() + 1, "( or variable name", "${expression[index.get()]}", expression)
            else -> {
                val variableName = parseName(variables, index, expression)
                ValueLeaf(variableName, negate)
            }
        }
    }

    private fun parseName(variables: MutableSet<String>, index: AtomicInteger, expression: String): String {
        var variableName = ""
        while (index.get() < expression.length) {
            when (val c = expression[index.getAndIncrement()]) {
                AND_SYMBOL, OR_SYMBOL, NEGATE_SYMBOL, ')' -> {
                    index.decrementAndGet()
                    return variableName.also { variables.add(it) }
                }
                else -> {
                    if (!"$c".matches("[a-zA-Z0-9\\-_]".toRegex()))
                        throw BooleanExpressionSyntaxError(
                                index.get(),
                                "variable name matches [a-zA-Z0-9\\-_]",
                                "$c", expression)
                    variableName += c
                }
            }
        }
        if (variableName.isEmpty())
            throw BooleanExpressionSyntaxError(
                    index.get(), "variable name", "empty name", expression)
        else return variableName.also { variables.add(it) }
    }

    private fun parseNegation(index: AtomicInteger, expression: String): Boolean {
        var negate = false
        while (index.get() < expression.length) {
            when (expression[index.getAndIncrement()]) {
                NEGATE_SYMBOL -> negate = !negate
                else -> {
                    index.decrementAndGet()
                    return negate
                }
            }
        }
        return negate
    }

    private fun parseOperator(index: AtomicInteger, expression: String): Operator? =
            when (expression[index.get()]) {
                AND_SYMBOL -> Operator.AND
                OR_SYMBOL -> Operator.OR
                else -> null
            }
}