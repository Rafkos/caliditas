/**
 * Copyright (C) 2018 Rafał Kosyl admin@rafkos.net
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package net.rafkos.utils.extjson

import java.util.regex.Matcher
import java.util.regex.Pattern

object ExtJson {
    private val multilineStringRegex = Pattern.compile("(?:\"{3}[\\s]*([\\S\\s]*?)[\\s]*\"{3})")

    /**
     * Parses extJson to normal Json format.
     */
    fun toJson(extJson: String): String {
        val matcher = multilineStringRegex.matcher(extJson)
        val buffer = StringBuffer(extJson.length)
        while (matcher.find()) {
            var inline = matcher.group(0)
                    .replace("\n", "\\n")
                    .replace("\t", "\\t")
                    .replace("\r", "\\r")
            inline = inline.substring(2, inline.length - 2)
            matcher.appendReplacement(buffer, Matcher.quoteReplacement(inline))
        }
        matcher.appendTail(buffer)
        return buffer.toString()
    }
}