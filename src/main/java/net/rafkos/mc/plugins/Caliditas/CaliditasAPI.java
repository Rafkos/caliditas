package net.rafkos.mc.plugins.Caliditas;

import net.rafkos.mc.plugins.Caliditas.loaders.LoadersManagerHelper;
import org.bukkit.World;
import org.bukkit.entity.Player;
import java.util.Set;

/**
 * Simple API for basic Caliditas mechanics.
 */
public class CaliditasAPI
{
	/**
	 * Registers factor in given world.
	 * @param f Factor to register.
	 * @param w Target world.
	 * @return Returns true if factor registered correctly.
	 */
	public static boolean registerFactor(Factor f, World w)
	{
		return LoadersManagerHelper.INSTANCE.getFactorsLoader(w).load(f);
	}

	/**
	 * Unregisters factor from given world.
	 * @param f Factor to unregister.
	 * @param w Target world.
	 * @return Returns true if factor was removed.
	 */
	public static boolean unregisterFactor(Factor f, World w)
	{
		return LoadersManagerHelper.INSTANCE.getFactorsLoader(w).getFactors().remove(f);
	}

	/**
	 * @param w Target world.
	 * @return Returns all factors of given world.
	 */
	public static Set<Factor> getFactors(World w)
	{
		return LoadersManagerHelper.INSTANCE.getFactorsLoader(w).getFactors();
	}

	/**
	 * @param p Target player.
	 * @param unit The unit of the temperature.
	 * @return Returns player temperature in given unit.
	 */
	public static double getPlayerTemperature(Player p, TemperatureUnit unit)
	{
		return LoadersManagerHelper.INSTANCE.getPlayerDataLoader().getPlayerData(p).getLastTemperature();
	}

	/**
	 * Applies specified temperature to given player.
	 * @param p Target player.
	 * @param temp Temperature.
	 * @param unit Temperature unit.
	 */
	public static void setPlayerTemperature(Player p, Double temp, TemperatureUnit unit)
	{
		PlayerData pd = LoadersManagerHelper.INSTANCE.getPlayerDataLoader().getPlayerData(p);
		pd.setTemperature(Config.INSTANCE.getTemperatureConverters().get(unit).convertToMachine(temp));
	}

	/**
	 * @param p Target player.
	 * @return Returns temperature state of given player.
	 */
	public static TemperatureState getPlayerTemperatureState(Player p)
	{
		return LoadersManagerHelper.INSTANCE.getPlayerDataLoader().getPlayerData(p).getState();
	}

	/**
	 * Enables / disables player's temperature immune status.
	 * @param p Target Player.
	 * @param flag Immune - true / vulnerable - false.
	 */
	public static void setPlayerImmuneInternally(Player p, boolean flag)
	{
		LoadersManagerHelper.INSTANCE.getPlayerDataLoader().getPlayerData(p).setTemperatureImmuneInternally(flag);
	}

	/**
	 * @param p Target player/
	 * @return Returns if player is immune to temperature internally.
	 * Which means that the Caliditas was disabled for this player using /cali immune command or player has no permissions.
	 */
	public static boolean isPlayerImmuneInternally(Player p)
	{
		return LoadersManagerHelper.INSTANCE.getPlayerDataLoader().getPlayerData(p).getTemperatureImmuneInternally();
	}

	/**
	 * @param p Target player/
	 * @return Returns if player is currently immune to the temperature either
	 * by active gamemode (e.g. creative) or because of player's world being banned from Caliditas.
	 */
	public static boolean isPlayerImmune(Player p)
	{
		return LoadersManagerHelper.INSTANCE.getPlayerDataLoader().getPlayerData(p).isTemperatureImmune();
	}

	/**
	 * @param p Target player.
	 * @return Returns current flags of player.
	 */
	public static Flag[] getPlayerFlags(Player p)
	{
		return (Flag[]) LoadersManagerHelper.INSTANCE.getPlayerDataLoader().getPlayerData(p).getFlags().toArray();
	}

	/**
	 * Adds flags to given player.
	 * @param p Target player.
	 * @param flags Flags to be added.
	 */
	public static void addFlagsToPlayer(Player p, Flag... flags)
	{
		PlayerData pd = LoadersManagerHelper.INSTANCE.getPlayerDataLoader().getPlayerData(p);
		for (Flag flag : flags)
		{
			pd.addFlag(flag);
		}
	}
}
